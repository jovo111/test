/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : test1

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 17/12/2019 17:39:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pms_opt_log
-- ----------------------------
DROP TABLE IF EXISTS `pms_opt_log`;
CREATE TABLE `pms_opt_log`  (
  `log_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `log_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志类型;INFO,ERROR',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员名称',
  `opt_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作ip',
  `opt_browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器名称',
  `opt_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求地址',
  `opt_os` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统名称',
  `opt_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用方法名',
  `opt_params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调用参数',
  `opt_exception` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异常信息',
  `opt_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作信息',
  `opt_time` int(11) NULL DEFAULT NULL COMMENT '请求耗时',
  `createtime` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户操作记录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pms_permission
-- ----------------------------
DROP TABLE IF EXISTS `pms_permission`;
CREATE TABLE `pms_permission`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '目录ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目录名称',
  `component_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名称',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '路径',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(1) NULL DEFAULT NULL COMMENT '排序',
  `pid` int(1) NULL DEFAULT NULL COMMENT '上级目录ID',
  `hidden` bit(1) NULL DEFAULT NULL COMMENT '是否隐藏',
  `cache` bit(1) NULL DEFAULT NULL COMMENT '是否开启vue 路由缓存',
  `i_frame` bit(1) NULL DEFAULT NULL COMMENT '是否是外链，0:否，1:是',
  `type` int(11) NULL DEFAULT NULL COMMENT '权限类型，0:顶级目录，1:子目录，2:按钮或者其他显示控件',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `pms_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限code',
  `editor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createtime` int(11) NULL DEFAULT NULL,
  `updatetime` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '前端菜单表，对应vue 的router组件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pms_permission
-- ----------------------------
INSERT INTO `pms_permission` VALUES (1, '系统管理', NULL, NULL, 'system', 'system', 1, 0, b'0', b'0', b'0', 0, NULL, 'system', 'admin', 'jovo', 1575008505, 1576222825);
INSERT INTO `pms_permission` VALUES (2, '用户管理', 'User', 'system/user/index', 'user', 'user', 2, 1, b'0', b'1', b'0', 1, NULL, 'system:user', 'jovo', 'jovo', 1575008660, 1575008660);
INSERT INTO `pms_permission` VALUES (3, '角色管理', 'Role', 'system/role/index', 'role', 'role', 3, 1, b'0', b'1', b'0', 1, NULL, 'system:role', 'jovo', 'jovo', 1575008819, 1575008819);
INSERT INTO `pms_permission` VALUES (4, '权限管理', 'Permission', 'system/permission/index', 'permission', 'permission', 4, 1, b'0', b'1', b'0', 1, NULL, 'system:pms', 'jovo', 'jovo', 1575008859, 1575008859);
INSERT INTO `pms_permission` VALUES (7, '添加用户', NULL, NULL, '', '', 1, 2, b'0', b'0', b'0', 2, NULL, 'system:user:create', 'jovo', 'jovo', 1575008859, 1575008860);
INSERT INTO `pms_permission` VALUES (8, '删除用户', NULL, NULL, '', '', 2, 2, b'0', b'0', b'0', 2, NULL, 'system:user:delete', 'jovo', 'jovo', 1575008859, 1575008860);
INSERT INTO `pms_permission` VALUES (9, '系统监控', '', '', 'monitor', 'monitor', 2, 0, b'0', b'0', b'0', 0, NULL, 'monitor', 'admin', 'admin', 1576221802, 1576222925);
INSERT INTO `pms_permission` VALUES (10, '用户列表', '', '', '', '', 3, 2, b'0', b'0', b'0', 2, NULL, 'system:user:list', 'admin', 'admin', 1576225583, 1576225667);
INSERT INTO `pms_permission` VALUES (11, '角色列表', '', '', '', '', 1, 3, b'0', b'0', b'0', 2, NULL, 'system:role:list', 'admin', 'admin', 1576225708, 1576225708);
INSERT INTO `pms_permission` VALUES (12, '添加角色', '', '', '', '', 2, 3, b'0', b'0', b'0', 2, NULL, 'system:role:create', 'admin', 'admin', 1576225752, 1576225752);
INSERT INTO `pms_permission` VALUES (13, '删除角色', '', '', '', '', 3, 3, b'0', b'0', b'0', 2, NULL, 'system:role:delete', 'admin', 'admin', 1576225806, 1576225806);
INSERT INTO `pms_permission` VALUES (14, '权限列表', '', '', '', '', 1, 4, b'0', b'0', b'0', 2, NULL, 'system:pms:list', 'admin', 'admin', 1576225860, 1576225860);
INSERT INTO `pms_permission` VALUES (15, '权限添加', '', '', '', '', 2, 4, b'0', b'0', b'0', 2, NULL, 'system:pms:create', 'admin', 'admin', 1576225902, 1576225902);
INSERT INTO `pms_permission` VALUES (16, '权限删除', '', '', '', '', 3, 4, b'0', b'0', b'0', 2, NULL, 'system:pms:delete', 'admin', 'admin', 1576225929, 1576477182);
INSERT INTO `pms_permission` VALUES (17, '操作日志', '操作日志', 'monitor/log/index', 'log', 'log', 1, 9, b'0', b'1', b'0', 1, NULL, 'monitor:log', 'admin', 'admin', 1576483582, 1576483687);

-- ----------------------------
-- Table structure for pms_role
-- ----------------------------
DROP TABLE IF EXISTS `pms_role`;
CREATE TABLE `pms_role`  (
  `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `status` bit(1) NULL DEFAULT b'0' COMMENT '是否启用',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `createtime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `creater` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `updatetime` int(11) NULL DEFAULT NULL COMMENT '修改时间',
  `editor` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `role_code` bit(1) NULL DEFAULT b'0' COMMENT '判断是否是超管，是-0，否-1',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pms_role
-- ----------------------------
INSERT INTO `pms_role` VALUES (1, 'admin', b'1', '超管', 1571749703, 'jovo', 1571749703, 'jovo', b'1');
INSERT INTO `pms_role` VALUES (2, 'user', b'1', '测试', 1571749703, 'jovo', 1576226074, 'jovo', b'0');
INSERT INTO `pms_role` VALUES (3, 'visitor', b'1', '游客', 1576225292, 'admin', 1576477067, 'admin', b'0');

-- ----------------------------
-- Table structure for pms_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `pms_role_menu`;
CREATE TABLE `pms_role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目录ID',
  `status` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `createtime` int(11) NULL DEFAULT NULL,
  `updatetime` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pms_role_menu
-- ----------------------------
INSERT INTO `pms_role_menu` VALUES (1, 1, '1,2,3,4,5', 1, 1575010941, NULL);
INSERT INTO `pms_role_menu` VALUES (2, 2, '1,2,3,4,5', 1, NULL, NULL);

-- ----------------------------
-- Table structure for pms_role_pms
-- ----------------------------
DROP TABLE IF EXISTS `pms_role_pms`;
CREATE TABLE `pms_role_pms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  `menus` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限列表',
  `createtime` int(11) NULL DEFAULT NULL,
  `updatetime` int(11) NULL DEFAULT NULL,
  `creater` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `editor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pms_role_pms
-- ----------------------------
INSERT INTO `pms_role_pms` VALUES (1, 1, '1,2,7,8,10,3,11,12,13,4,14,15,16,9,17', 1571819392, 1571819392, 'jovo', 'jovo', '超管权限');
INSERT INTO `pms_role_pms` VALUES (2, 2, '2,7,8,10', 1571819392, 1571823897, 'jovo', 'jovo', '管理员1权限');
INSERT INTO `pms_role_pms` VALUES (3, 3, '10,11,14', 1571819392, 1571819392, 'jovo', 'jovo', '管理员2权限');

-- ----------------------------
-- Table structure for pms_role_user
-- ----------------------------
DROP TABLE IF EXISTS `pms_role_user`;
CREATE TABLE `pms_role_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NULL DEFAULT NULL COMMENT '用户列表',
  `roles` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拥有角色',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `createtime` int(11) NULL DEFAULT NULL,
  `updatetime` int(11) NULL DEFAULT NULL,
  `editor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `creater` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pms_role_user
-- ----------------------------
INSERT INTO `pms_role_user` VALUES (1, 1, '1', '超管', 1571829954, 1571829954, 'admin', 'jovo');
INSERT INTO `pms_role_user` VALUES (2, 2, '3', '', 1576491213, 1576491213, 'admin', 'admin');

-- ----------------------------
-- Table structure for pms_user
-- ----------------------------
DROP TABLE IF EXISTS `pms_user`;
CREATE TABLE `pms_user`  (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户表',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `status` bit(1) NULL DEFAULT b'0' COMMENT '用户状态',
  `createtime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `createip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册ip地址',
  `last_login_time` int(11) NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录ip地址',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pms_user
-- ----------------------------
INSERT INTO `pms_user` VALUES (1, 'admin', 'admin', '160ecce9183eccd247487cade4beff6c', 'hello', b'1', 1569751904, '127.0.0.1', 1569751904, '127.0.0.1');
INSERT INTO `pms_user` VALUES (2, 'tongchaofei', NULL, '160ecce9183eccd247487cade4beff6c', NULL, b'1', 1576491213, '0:0:0:0:0:0:0:1', 1576491213, '0:0:0:0:0:0:0:1');

SET FOREIGN_KEY_CHECKS = 1;
