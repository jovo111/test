package com.jovo.common.base;

/**
 * @ClassName BaseRedis
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-14 18:13
 * @Version V1.0
 */
public interface BaseRedis {

    Object get(Object key);

    void set(String key,Object value);

    void del(String key);
}
