package com.jovo.common.base;

/**
 * @ClassName BaseResultConstant
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-26 16:52
 * @Version V1.0
 */
public enum BaseResultConstant {

    /**
     * @Author Jovo
     * @Description 成功
     * @Date 16:53 2019-10-26
     * @Param
     * @return
     **/
    SUCCESS(0, "success"),

    /**
     * @Author Jovo
     * @Description 失败
     * @Date 16:54 2019-10-26
     * @Param
     * @return
     **/
    FAILED(1, "failed"),

    TIMEOUT(101, "request timed out"),

    UNAUTHOR(102, "auth error");

    public int code;

    public String msg;

    BaseResultConstant(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
