package com.jovo.common.base;

/**
 * @ClassName BaseResult
 * @Description 统一返回结果集
 * @Author Jovo
 * @Date 2019-10-26 16:36
 * @Version V1.0
 */
public class BaseResult {

    /**
     * @Author Jovo
     * @Description 状态码,0成功 其他为失败
     * @Date 16:37 2019-10-26
     * @Param
     * @return
     **/
    public int code;

    /**
     * @Author Jovo
     * @Description 成功为success,其他为失败原因
     * @Date 16:42 2019-10-26
     * @Param
     * @return
     **/
    public String msg;

    /**
     * @Author Jovo
     * @Description 数据结果集
     * @Date 16:42 2019-10-26
     * @Param
     * @return
     **/
    public Object data;

    public BaseResult(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
