package com.jovo.common.base;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jovo.common.util.ThrowableUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.springframework.dao.QueryTimeoutException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * 控制器基类
 *
 * @author jovo
 * @date 2019/09/20
 */
@Slf4j
public abstract class BaseController {

    @ExceptionHandler
    public String exceptionHandler(Exception exception){
        log.error(ThrowableUtil.getStackTrace(exception));

        if(exception instanceof QueryTimeoutException){
            return JSONObject.toJSONString(new BaseResult(BaseResultConstant.TIMEOUT.getCode(),BaseResultConstant.TIMEOUT.getMsg(),"查询超时"));
        }
        if(exception instanceof AuthenticationException){
            return JSONObject.toJSONString(new BaseResult(BaseResultConstant.UNAUTHOR.getCode(),BaseResultConstant.UNAUTHOR.getMsg(),"会话失效，请重试"));
        }

        return JSONObject.toJSONString(new BaseResult(BaseResultConstant.FAILED.getCode(),BaseResultConstant.FAILED.getMsg(),"系统出了点问题,攻城狮正在加班加点维修"));
    }

}
