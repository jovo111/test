package com.jovo.common.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;

/**
 * @ClassName SecurityUtil
 * @Description 获取当前登录的用户
 * @Author Jovo
 * @Date 2019-12-17 9:32
 * @Version V1.0
 */
@Slf4j
public class SecurityUtil {

    public static Object getUserDetails() {
        Object obj = null;
        try {
            obj = SecurityUtils.getSubject().getPrincipal();
        } catch (UnauthorizedException e) {
            log.error("登录状态过期");
        }

        return obj;
    }
    
    /**
     * @Author Jovo
     * @Description 获取系统用户名称
     * @Date 9:44 2019-12-17
     * @Param []
     * @return java.lang.String
     **/
    public static String getUserName() {
        Object obj = getUserDetails();
        String jsonStr = JSONObject.toJSONString(obj);
        JSONObject json = JSONObject.parseObject(jsonStr);
        return json.getString("userName");
    }

    public static Integer getUserId(){
        Object obj = getUserDetails();
        String jsonStr = JSONObject.toJSONString(obj);
        JSONObject json = JSONObject.parseObject(jsonStr);
        return json.getInteger("userId");
    }
}
