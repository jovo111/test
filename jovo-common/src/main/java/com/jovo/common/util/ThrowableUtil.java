package com.jovo.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @ClassName ThrowableUtil
 * @Description 异常工具类
 * @Author Jovo
 * @Date 2019-12-24 16:15
 * @Version V1.0
 */
public class ThrowableUtil {

    /**
     * @Author Jovo
     * @Description 获取堆栈信息
     * @Date 16:16 2019-12-24
     * @Param [throwable]
     * @return java.lang.String
     **/
    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }
}
