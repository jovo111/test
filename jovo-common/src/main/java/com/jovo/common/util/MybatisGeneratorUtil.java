package com.jovo.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.FileTemplateResolver;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class MybatisGeneratorUtil {

    // generatorConfig模板路径
    private static String generatorConfig_tpl = "/template/generatorConfig.tpl";
    // Service模板路径
    private static String service_tpl = "/template/Service.tpl";
    // ServiceMock模板路径
    private static String serviceMock_tpl = "/template/ServiceMock.tpl";
    // ServiceImpl模板路径
    private static String serviceImpl_tpl = "/template/ServiceImpl.tpl";

    private static TemplateEngine templateEngine;


    /**
     * 根据模板生成generatorConfig.xml文件
     *
     * @param jdbcDriver   驱动路径
     * @param jdbcUrl      链接
     * @param jdbcUsername 帐号
     * @param jdbcPassword 密码
     * @param module       项目模块
     * @param database     数据库
     * @param tablePrefix  表前缀
     * @param packageName  包名
     */
    public static void generatorCode(String jdbcDriver, String jdbcUrl, String jdbcUsername, String jdbcPassword, String module, String database, String tablePrefix, String packageName, Map<String, String> lastInsertIdTables) throws Exception {

        String os = System.getProperty("os.name");
        String targetProject = module + "/" + module + "-dao";
        String basePath = MybatisGeneratorUtil.class.getResource("/").getPath().replace("/target/classes/", "").replace(targetProject, "");
        if (os.toLowerCase().startsWith("win")) {
            generatorConfig_tpl = MybatisGeneratorUtil.class.getResource(generatorConfig_tpl).getPath().replaceFirst("/", "");
            service_tpl = MybatisGeneratorUtil.class.getResource(service_tpl).getPath().replaceFirst("/", "");
            serviceMock_tpl = MybatisGeneratorUtil.class.getResource(serviceMock_tpl).getPath().replaceFirst("/", "");
            serviceImpl_tpl = MybatisGeneratorUtil.class.getResource(serviceImpl_tpl).getPath().replaceFirst("/", "");
            basePath = basePath.replaceFirst("/", "");
        } else {
            generatorConfig_tpl = MybatisGeneratorUtil.class.getResource(generatorConfig_tpl).getPath();
            service_tpl = MybatisGeneratorUtil.class.getResource(service_tpl).getPath();
            serviceMock_tpl = MybatisGeneratorUtil.class.getResource(serviceMock_tpl).getPath();
            serviceImpl_tpl = MybatisGeneratorUtil.class.getResource(serviceImpl_tpl).getPath();
        }

        Context context = new Context();

        String generatorConfigXml = MybatisGeneratorUtil.class.getResource("/").getPath().replace("/target/classes/", "") + "/src/main/resources/generatorConfig.xml";
        targetProject = basePath + targetProject;
        String sql = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = '" + database + "' AND table_name LIKE '" + tablePrefix + "';";

        log.info("========== 开始生成generatorConfig.xml文件 ==========");
        List<Map<String, Object>> tables = new ArrayList<Map<String, Object>>();
        try {

            Map<String, Object> table;

            // 查询定制前缀项目的所有表
            JdbcUtil jdbcUtil = new JdbcUtil(jdbcDriver, jdbcUrl, jdbcUsername, jdbcPassword);
            List<Map> result = jdbcUtil.selectByParams(sql, null);
            for (Map map : result) {
                log.info("" + map.get("TABLE_NAME"));
                table = new HashMap<String, Object>(2);
                table.put("table_name", map.get("TABLE_NAME"));
                table.put("model_name", StringUtil.lineToHump(ObjectUtils.toString(map.get("TABLE_NAME"))));
                tables.add(table);
            }
            jdbcUtil.release();

            String targetProjectSqlMap = basePath + module + "/" + module + "-service";
            context.setVariable("tables", tables);
            context.setVariable("jdbcDriver", jdbcDriver);
            context.setVariable("jdbcUrl", jdbcUrl.replaceAll("&","&amp;"));
            context.setVariable("jdbcUsername", jdbcUsername);
            context.setVariable("jdbcPassword", jdbcPassword);
            context.setVariable("generator_javaModelGenerator_targetPackage", packageName + ".dao.model");
            context.setVariable("generator_sqlMapGenerator_targetPackage", packageName + ".dao.mapper");
            context.setVariable("generator_javaClientGenerator_targetPackage", packageName + ".dao.mapper");
            context.setVariable("targetProject", targetProject);
            context.setVariable("targetProject_sqlMap", targetProjectSqlMap);
            context.setVariable("generator_jdbc_password", jdbcPassword);
            context.setVariable("last_insert_id_tables", lastInsertIdTables);
            generator(generatorConfig_tpl, generatorConfigXml, context);
            // 删除旧代码
//            deleteDir(new File(targetProject + "/src/main/java/" + packageName.replaceAll("\\.", "/") + "/dao/model"));
//            deleteDir(new File(targetProject + "/src/main/java/" + packageName.replaceAll("\\.", "/") + "/dao/mapper"));
//            deleteDir(new File(targetProjectSqlMap + "/src/main/java/" + packageName.replaceAll("\\.", "/") + "/dao/mapper"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("========== 结束生成generatorConfig.xml文件 ==========");


        log.info("========== 开始运行MybatisGenerator ==========");
        List<String> warnings = new ArrayList<String>();
        File configFile = new File(generatorConfigXml);
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(true);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
        for (String warning : warnings) {
            log.info(warning);
        }
        log.info("========== 结束运行MybatisGenerator ==========");

        log.info("========== 开始生成Service ==========");
        String ctime = new SimpleDateFormat("yyyy/M/d").format(new Date());
        String servicePath = basePath + module + "/" + module + "-api" + "/src/main/java/" + packageName.replaceAll("\\.", "/") + "/api";
        String serviceImplPath = basePath + module + "/" + module + "-service" + "/src/main/java/" + packageName.replaceAll("\\.", "/") + "/service/impl";

        log.info(servicePath);
        log.info(serviceImplPath);

        for (Map table : tables) {
            String model = StringUtil.lineToHump(ObjectUtils.toString(table.get("table_name")));
            String service = servicePath + "/" + model + "Service.java";
            String serviceMock = servicePath + "/" + model + "ServiceMock.java";
            String serviceImpl = serviceImplPath + "/" + model + "ServiceImpl.java";

            context.setVariable("package_name", packageName);
            context.setVariable("model", model);
            context.setVariable("mapper", StringUtil.toLowerCaseFirstOne(model));
            context.setVariable("ctime", ctime);

            // 创建java文件
            createFile(service);
            createFile(serviceMock);
            createFile(serviceImpl);

            //生成java文件
            generator(service_tpl, service, context);
            generator(serviceMock_tpl, serviceMock, context);
            generator(serviceImpl_tpl, serviceImpl, context);
        }
        log.info("========== 结束生成Service ==========");
    }

    private static void generator(String template, String path, Context context) throws IOException {

        Writer writer = new FileWriter(path);
        FileTemplateResolver templateResolver = new FileTemplateResolver();
        templateResolver.setPrefix("");
        templateResolver.setTemplateMode("TEXT");
        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        writer.write(templateEngine.process(template, context));
        writer.close();
    }

    private static void createFile(String path) {
        File file = new File(path);

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    // 递归删除非空文件夹
    public static void deleteDir(File dir) {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                deleteDir(files[i]);
            }
        }
        dir.delete();
    }
}
