package [(${package_name})].service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import [(${package_name})].dao.mapper.[(${model})]Mapper;
import [(${package_name})].dao.model.[(${model})];
import [(${package_name})].dao.model.[(${model})]Example;
import [(${package_name})].api.[(${model})]Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
* [(${model})]Service实现
* Created by jovo on [(${ctime})].
*/
@Slf4j
@Service
@Component
@Transactional
@BaseService
public class [(${model})]ServiceImpl extends BaseServiceImpl<[(${model})]Mapper, [(${model})], [(${model})]Example> implements [(${model})]Service {

    @Autowired
    [(${model})]Mapper [(${mapper})]Mapper;

}