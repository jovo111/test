package [(${groupId})].[(${module})].[(${layer})].controller;

import [(${groupId})].common.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.slf4j.Slf4j;

/**
 * [(${modelname})]controller
 * Created by jovo on [(${ctime})].
 */
@Slf4j
@Controller
@RequestMapping("/manage")
@Api(value = "[(${modelname})]控制器", description = "[(${modelname})]管理")
public class [(${model})]Controller extends BaseController {

}