package [(${package_name})].api;

import com.jovo.common.base.BaseServiceMock;
import [(${package_name})].dao.mapper.[(${model})]Mapper;
import [(${package_name})].dao.model.[(${model})];
import [(${package_name})].dao.model.[(${model})]Example;
import lombok.extern.slf4j.Slf4j;

/**
* 降级实现[(${model})]Service接口
* Created by jovo on [(${ctime})].
*/
@Slf4j
public class [(${model})]ServiceMock extends BaseServiceMock<[(${model})]Mapper, [(${model})], [(${model})]Example> implements [(${model})]Service {

}
