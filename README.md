此项目为测试项目，用于 dokcer + jenkins + gitlab 项目的测试。
工作流程：
1.开发人员提交代码到Git版本仓库；
2.Jenkins人工/定时触发项目构建；
3.Jenkins拉取代码、代码编码、打包镜像、推送到镜像仓库；
4.Jenkins在Docker主机创建容器并发布。
