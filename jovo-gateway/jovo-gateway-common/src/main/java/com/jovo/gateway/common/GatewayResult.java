package com.jovo.gateway.common;

import com.jovo.common.base.BaseResult;

/**
 * @ClassName SsoResult
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-26 16:33
 * @Version V1.0
 */
public class GatewayResult extends BaseResult {

    public GatewayResult(GatewayResultConstant gatewayResultConstant, Object data) {
        super(gatewayResultConstant.getCode(), gatewayResultConstant.getMsg(), data);
    }
}
