package com.jovo.gateway.common;

/**
 * @ClassName BaseResultConstant
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-26 16:52
 * @Version V1.0
 */
public enum GatewayResultConstant {

    /**
     * @Author Jovo
     * @Description 成功
     * @Date 16:54 2019-10-26
     * @Param
     * @return
     **/
    SUCCESS(0, "success"),

    /**
     * @Author Jovo
     * @Description 失败
     * @Date 16:53 2019-10-26
     * @Param
     * @return
     **/
    FAILED(1, "failed"),

    /**
     * 用户名不能为空
     */
    EMPTY_USERNAME(10001, "Username cannot be empty"),

    /**
     * 密码不能为空
     */
    EMPTY_PASSWORD(10002, "Password cannot be empty"),

    /**
     * 帐号不存在
     */
    INVALID_USERNAME(10003, "Account does not exist"),

    /**
     * 密码错误
     */
    INVALID_PASSWORD(10004, "Password error"),

    /**
     * 无效帐号
     */
    INVALID_ACCOUNT(10005, "Invalid account"),

    EMPTY_VCODE (10006, "VerfityCode cannot be empty"),

    EMPTY_UUID(10007, "UUID cannot be empty"),

    ERROR_VCODE(10008, "VerfityCode error"),

    ONLINE(10009, "User is online");

    public int code;

    public String msg;

    GatewayResultConstant(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }}
