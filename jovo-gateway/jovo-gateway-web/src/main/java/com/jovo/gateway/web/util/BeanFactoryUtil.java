package com.jovo.gateway.web.util;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jovo.gateway.api.PmsOptLogService;
import com.jovo.gateway.api.PmsRolePmsService;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.api.PmsUserService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @ClassName BeanFactoryUtil
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-05 15:10
 * @Version V1.0
 */
@Component
public class BeanFactoryUtil {

    @Reference
    PmsUserService pmsUserService;

    @Bean(name = "pmsUserService")
    public PmsUserService getPmsUserService() {
        return pmsUserService;
    }

    @Reference
    PmsRolePmsService pmsRolePmsService;

    @Bean(name = "pmsRolePmsService")
    public PmsRolePmsService getPmsRolePmsService() {
        return pmsRolePmsService;
    }

    @Reference
    PmsRoleUserService pmsRoleUserService;

    @Bean(name = "pmsRoleUserService")
    public PmsRoleUserService getPmsRoleUserService() {
        return pmsRoleUserService;
    }
}
