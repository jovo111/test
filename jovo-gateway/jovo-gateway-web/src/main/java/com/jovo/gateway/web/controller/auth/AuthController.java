package com.jovo.gateway.web.controller.auth;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.IdUtil;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.jovo.common.annotation.Log;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.web.model.ImgResult;
import com.jovo.gateway.web.util.VerifyCodeUtil;
import com.jovo.gateway.web.vo.auth.AuthInfoVo;
import com.jovo.gateway.web.vo.auth.AuthUserVo;
import com.jovo.gateway.web.shiro.session.SsoSessionDao;
import com.jovo.gateway.web.shiro.session.SsoSession;
import com.jovo.gateway.web.util.RedisUtil;
import com.jovo.gateway.web.util.ShiroUtil;
import com.jovo.gateway.web.util.VCodeRedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.BooleanUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

/**
 * @ClassName AccountController
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-25 16:36
 * @Version V1.0
 */
@Slf4j
@RestController
@RequestMapping("/auth")
@Api(value = "单点登录管理", description = "单点登录管理")
public class AuthController extends BaseController {

    @Resource
    private RedisUtil redisUtil;

    @Reference
    private PmsRoleUserService pmsRoleUserService;

    @Log("用户登录")
    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    @ResponseBody
    public Object login(@RequestBody AuthUserVo authUserVo,
                        HttpServletResponse response) {

        if (StringUtils.isBlank(authUserVo.getUsername())) {
            return new GatewayResult(GatewayResultConstant.EMPTY_USERNAME, "账号不能为空");
        }

        if (StringUtils.isBlank(authUserVo.getPassword())) {
            return new GatewayResult(GatewayResultConstant.EMPTY_PASSWORD, "密码不能为空");
        }

        if (StringUtils.isBlank(authUserVo.getUuid())) {
            return new GatewayResult(GatewayResultConstant.EMPTY_UUID, "UUID为空,请联系管理员");
        }

        String vCode = (String) redisUtil.get(VCodeRedisUtil.PREFIX + authUserVo.getUuid());
        if (StringUtils.isBlank(authUserVo.getCode()) || !authUserVo.getCode().equalsIgnoreCase(vCode)) {
            return new GatewayResult(GatewayResultConstant.ERROR_VCODE, "验证码错误请重试");
        }

        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        String sessionId = session.getId().toString();

        // 使用shiro认证
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(authUserVo.getUsername(), authUserVo.getPassword());
        try {
            if (BooleanUtils.toBoolean(authUserVo.isRememberMe())) {
                usernamePasswordToken.setRememberMe(true);
            } else {
                usernamePasswordToken.setRememberMe(false);
            }
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException e) {
            return new GatewayResult(GatewayResultConstant.INVALID_USERNAME, "帐号不存在！");
        } catch (IncorrectCredentialsException e) {
            return new GatewayResult(GatewayResultConstant.INVALID_PASSWORD, "密码错误！");
        } catch (LockedAccountException e) {
            return new GatewayResult(GatewayResultConstant.INVALID_ACCOUNT, "帐号已锁定！");
        }

        // 默认验证帐号密码正确，创建code
        String token = IdUtil.simpleUUID();
        // 全局会话的token
        redisUtil.set(ShiroUtil.SESSIONID_COOKIE_NAME + ":" + token, sessionId, ShiroUtil.EXPIRE_SECONDS);

        PmsUser pmsUser = (PmsUser) subject.getPrincipal();
        Set<String> pmsPermissions = pmsRoleUserService.selectPmsPermissionsByUid(pmsUser.getUserId());
        if (pmsUser.getUserName().equals("admin")) pmsPermissions.add("ADMIN");
        return new GatewayResult(GatewayResultConstant.SUCCESS, new AuthInfoVo(token, pmsUser, pmsPermissions));

    }

    @Log("用户注销")
    @PostMapping("/logout")
    @ResponseBody
    @ApiOperation(value = "用户注销")
    public Object logout(@RequestBody AuthUserVo authUserVo) {
        // 删除sessionCookieId
        String key = ShiroUtil.SESSIONID_COOKIE_NAME + ":" + authUserVo.getToken();
        redisUtil.del(key);

        SecurityUtils.getSubject().logout();
        return new GatewayResult(GatewayResultConstant.SUCCESS, "注销成功");
    }

    /**
     * @return java.lang.Object
     * @Author Jovo
     * @Description 获取验证码
     * @Date 16:07 2019-12-17
     * @Param [response]
     **/
    @GetMapping("/vCode")
    @ResponseBody
    @ApiOperation(value = "获取验证码")
    public Object getCode(HttpServletResponse response) throws IOException {

        //生成随机字串
        String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
        String uuid = IdUtil.simpleUUID();
        redisUtil.set(VCodeRedisUtil.PREFIX + uuid, verifyCode, VCodeRedisUtil.MAX_AGE);

        // 生成图片
        int w = 111, h = 36;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        VerifyCodeUtil.outputImage(w, h, stream, verifyCode);
        try {
            return new GatewayResult(GatewayResultConstant.SUCCESS, new ImgResult(Base64.encode(stream.toByteArray()), uuid));
        } catch (Exception e) {
            e.printStackTrace();
            return new GatewayResult(GatewayResultConstant.FAILED, "验证码生成失败,请联系管理员");
        } finally {
            stream.close();
        }
    }
}
