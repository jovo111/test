package com.jovo.gateway.web.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ShiroUtil {

    public static String LOGINURL = PropertiesFileUtil.getInstance().get("shiro.loginUrl");

    public static String INDEXURL = PropertiesFileUtil.getInstance().get("shiro.indexUrl");

    public static String HASH_ALGORITHM_NAME = PropertiesFileUtil.getInstance().get("shiro.hashAlgorithmName");

    public static int HASH_ITERATIONS = PropertiesFileUtil.getInstance().getInt("shiro.hashIterations");

    public static int COOKIE_MAX_AGE = PropertiesFileUtil.getInstance().getInt("shiro.cookieMaxAge");

    public static int GLOBAL_SESSION_TIMEOUT = PropertiesFileUtil.getInstance().getInt("shiro.globalSessionTimeout");

    public static String SESSIONID_COOKIE_NAME = PropertiesFileUtil.getInstance().get("shiro.sessionidCookieName");

    public static String SHIRO_SESSION_PREFIX = PropertiesFileUtil.getInstance().get("shiro.sessionPrefix");

    public static String SHIRO_CACHE_PREFIX = PropertiesFileUtil.getInstance().get("shiro.cachePrefix");

    public static int EXPIRE_SECONDS = PropertiesFileUtil.getInstance().getInt("shiro.expireSeconds");

    public static String JOVO_SSO_CODE = PropertiesFileUtil.getInstance().get("shiro.ssoCode");
}
