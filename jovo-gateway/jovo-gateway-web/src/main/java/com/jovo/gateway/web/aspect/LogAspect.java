package com.jovo.gateway.web.aspect;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jovo.common.util.RequestUtil;
import com.jovo.common.util.SecurityUtil;
import com.jovo.gateway.api.PmsOptLogService;
import com.jovo.gateway.dao.model.PmsOptLog;
import com.jovo.gateway.web.util.SpringBeanFactoryUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @ClassName LogAspect
 * @Description TODO
 * @Author Jovo
 * @Date 2019-12-16 17:21
 * @Version V1.0
 */
@Component
@Aspect
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class LogAspect {

    @Reference
    private PmsOptLogService pmsOptLogService;

    private long currentTime = 0L;

    @Pointcut("@annotation(com.jovo.common.annotation.Log)")
    public void logPointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    /**
     * @return java.lang.Object
     * @Author Jovo
     * @Description 配置环绕通知, 使用在方法logPointcut()上注册的切入点
     * @Date 17:31 2019-12-16
     * @Param [joinPoint]
     **/
    @Around("logPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        currentTime = System.currentTimeMillis();
        result = joinPoint.proceed();
        Long nowTime = System.currentTimeMillis();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String IpAddr = RequestUtil.getIpAddr(request);
        PmsOptLog pmsOptLog = new PmsOptLog("INFO", getUsername(), IpAddr, "", RequestUtil.getBrowser(request), RequestUtil.getOsName(request), nowTime - currentTime, nowTime/1000);
        pmsOptLog = getPointInfo(pmsOptLog, joinPoint);
        pmsOptLogService.insert(pmsOptLog);
        return result;
    }

    /**
     * 配置异常通知
     *
     * @param joinPoint join point for advice
     * @param e         exception
     */
    @AfterThrowing(pointcut = "logPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        Long nowTime = System.currentTimeMillis();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String IpAddr = RequestUtil.getIpAddr(request);
        PmsOptLog pmsOptLog = new PmsOptLog("ERROR", getUsername(), IpAddr, "", RequestUtil.getBrowser(request), RequestUtil.getOsName(request), nowTime - currentTime, nowTime/1000);
        pmsOptLog = getPointInfo(pmsOptLog, (ProceedingJoinPoint) joinPoint);
        pmsOptLogService.insert(pmsOptLog);
    }

    public PmsOptLog getPointInfo(PmsOptLog pmsOptLog, ProceedingJoinPoint point) {

        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        com.jovo.common.annotation.Log optLog = method.getAnnotation(com.jovo.common.annotation.Log.class);

        //参数值
        Object[] argValues = point.getArgs();

        // 描述
        if (pmsOptLog != null) {
            pmsOptLog.setOptInfo(optLog.value());
        }
        assert pmsOptLog != null;

        String LOGINPATH = "login";
        String LOGOUTPATH = "logout";
        String userName = getUsername();
        if (LOGINPATH.equals(signature.getName()) || LOGOUTPATH.equals(signature.getName())) {
            try {
                assert argValues != null;
                String jsonStr = JSONObject.toJSONString(argValues[0]);
                userName = JSONObject.parseObject(jsonStr).getString("username");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        pmsOptLog.setUserName(userName);

        return pmsOptLog;
    }

    public String getUsername() {
        try {
            return SecurityUtil.getUserName();
        } catch (Exception e) {
            return "";
        }
    }

    public static void main(String [] args){
        String ip = "220.248.12.158";
        log.info(RequestUtil.getAddress(ip));
    }
}
