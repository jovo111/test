package com.jovo.gateway.web.shiro.session;

import org.apache.shiro.session.mgt.SimpleSession;

import java.io.Serializable;

/**
 * @ClassName ShiroSession
 * @Description 重写session
 * @Author Jovo
 * @Date 2019-11-06 16:13
 * @Version V1.0
 */
public class SsoSession extends SimpleSession implements Serializable {

    public static enum OnlineStatus {
        /**
         * 在线
         */
        on_line("在线"),

        /**
         * 离线
         */
        off_line("离线"),

        /**
         * 强制退出
         */
        force_logout("强制退出");

        private final String info;

        private OnlineStatus(String info) {
            this.info = info;
        }

        public String getInfo() {
            return info;
        }
    }

    // 用户浏览器类型
    private String userAgent;

    // 在线状态
    private OnlineStatus status = OnlineStatus.off_line;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public OnlineStatus getStatus() {
        return status;
    }

    public void setStatus(OnlineStatus status) {
        this.status = status;
    }
}
