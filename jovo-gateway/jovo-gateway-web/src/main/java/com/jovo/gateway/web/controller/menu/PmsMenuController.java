package com.jovo.gateway.web.controller.menu;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.jovo.common.annotation.Log;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsMenuService;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.web.util.PmsTreeUtil;
import com.jovo.gateway.web.vo.menu.MenuMetaVo;
import com.jovo.gateway.web.vo.menu.MenuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @ClassName MenuController
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-15 15:14
 * @Version V1.0
 */
@Slf4j
@Api(tags = "权限目录管理")
@RestController
@RequestMapping("/admin/menus")
@Transactional(rollbackFor = Exception.class)
public class PmsMenuController extends BaseController {

    @Reference
    private PmsMenuService pmsMenuService;

    @Reference
    private PmsRoleUserService pmsRoleUserService;

    @Resource
    private PmsTreeUtil pmsTreeUtil;

    @GetMapping("build")
    @ResponseBody
    public Object getMenusToUser() {

        PmsUser pmsUser = (PmsUser) SecurityUtils.getSubject().getPrincipal();
        List<PmsPermission> pmsPermissions = pmsRoleUserService.getPmsMenusByUserId(pmsUser.getUserId());

        List<PmsPermission> trees = pmsTreeUtil.buildTree(pmsPermissions);
        List<MenuVo> menus = buildMenus(trees);

        return new GatewayResult(GatewayResultConstant.SUCCESS, menus);
    }

    @GetMapping("tree")
    @ResponseBody
    public Object getMenusTree() {

        return new GatewayResult(GatewayResultConstant.SUCCESS, null);
    }

    @Log("添加目录")
    @PostMapping("")
    @ResponseBody
    @ApiOperation(value = "添加目录", notes = "添加目录")
    public Object add(PmsPermission pmsPermission) {

        long now = System.currentTimeMillis() / 1000;
        pmsPermission.setCreatetime(now);
        pmsPermission.setUpdatetime(now);
        log.info("------------------------PmsMenu:{}", JSON.toJSONString(pmsPermission));
        int count = pmsMenuService.insert(pmsPermission);

        if (count > 0) {
            return "SUCCESS";
        }

        return GatewayResultConstant.FAILED;
    }


    @DeleteMapping("/{menuId}")
    @ResponseBody
    @ApiOperation(value = "删除单个目录", notes = "删除")
    public Object del(@PathVariable("menuId") Integer menuId) {
        int count = pmsMenuService.delete(menuId);

        if (count > 0) {
            return "SUCCESS";
        }

        return GatewayResultConstant.FAILED;
    }

    @GetMapping("/{menuId}")
    @ResponseBody
    @ApiOperation(value = "根据menuId 查询单个目录信息", notes = "查询")
    public Object getMenu(@PathVariable("menuId") Integer menuId) {
        PmsPermission pmsPermission = pmsMenuService.selectPmsMenuByMenuId(menuId);

        return pmsPermission;
    }

    @PutMapping("")
    @ResponseBody
    @ApiOperation(value = "根据menuId 修改单个目录信息", notes = "修改")
    public Object update(PmsPermission pmsPermission) {
        long now = System.currentTimeMillis() / 1000;
        pmsPermission.setUpdatetime(now);
        log.info("------------------------PmsMenu:{}", JSON.toJSONString(pmsPermission));
        int count = pmsMenuService.update(pmsPermission);

        if (count > 0) {
            return GatewayResultConstant.SUCCESS;
        }

        return GatewayResultConstant.FAILED;
    }

    /**
     * @Author Jovo
     * @Description 生成vue 目录
     * @Date 9:58 2019-12-11
     * @Param [pmsPermissions]
     * @return java.util.List<com.jovo.gateway.web.vo.menu.MenuVo>
     **/
    public List<MenuVo> buildMenus(List<PmsPermission> pmsPermissions){
        List<MenuVo> menus = new LinkedList<>();
        pmsPermissions.forEach(menu -> {
            if (menu != null) {
                List<PmsPermission> childList = menu.getChildren();
                MenuVo menuVo = new MenuVo();
                menuVo.setName(ObjectUtil.isNotEmpty(menu.getComponentName()) ? menu.getComponentName() : menu.getMenuName());
                // 一级目录需要加斜杠，不然会报警告
                menuVo.setPath(menu.getPid() == 0 ? "/" + menu.getPath() : menu.getPath());
                menuVo.setHidden(menu.getHidden());

                if(!menu.getIFrame()){
                    if (menu.getPid() == 0) {
                        menuVo.setComponent(StrUtil.isEmpty(menu.getComponent()) ? "Layout" : menu.getComponent());
                    } else if (!StrUtil.isEmpty(menu.getComponent())) {
                        menuVo.setComponent(menu.getComponent());
                    }
                }

                menuVo.setMeta(new MenuMetaVo(menu.getMenuName(), menu.getIcon(), !menu.getCache()));
                if (childList != null && childList.size() != 0) {
                    menuVo.setAlwaysShow(true);
                    menuVo.setRedirect("noredirect");
                    menuVo.setChildren(buildMenus(childList));
                    // 处理是一级菜单并且没有子菜单的情况
                } else if (menu.getPid() == 0) {
                    MenuVo menuVo1 = new MenuVo();
                    menuVo1.setMeta(menuVo.getMeta());
                    // 非外链
                    if (!menu.getIFrame()) {
                        menuVo1.setPath("index");
                        menuVo1.setName(menuVo.getName());
                        menuVo1.setComponent(menuVo.getComponent());
                    } else {
                        menuVo1.setPath(menu.getPath());
                    }
                    menuVo.setName("");
                    menuVo.setMeta(new MenuMetaVo());
                    menuVo.setComponent("Layout");
                    List<MenuVo> list1 = new ArrayList<MenuVo>();
                    list1.add(menuVo1);
                    menuVo.setChildren(list1);
                }

                menus.add(menuVo);
            }
        });

        return menus;
    }

    public static void main(String [] args){
        List<String> myList = new CopyOnWriteArrayList<String>();
        myList.add( "1");
        myList.add( "2");
        myList.add( "3");
        myList.add( "4");
        myList.add( "5");

        Iterator<String> it = myList.iterator();

        while (it.hasNext()) {
            String value = it.next();
            if (value.equals( "3")) {
                myList.remove( "5");
                myList.add( "6");
                myList.add( "7");
            }
        }
        System. out.println( "List Value:" + myList.toString());
    }
}
