package com.jovo.gateway.web.vo.auth;

import com.jovo.gateway.dao.model.PmsUser;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

/**
 * @ClassName AuthInfoDto
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-13 18:15
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public class AuthInfoVo {

    private String token;

    private PmsUser user;

    private Set<String> pms;
}
