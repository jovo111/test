package com.jovo.gateway.web.controller.permission;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jovo.common.annotation.Log;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsPermissionService;
import com.jovo.gateway.api.PmsRoleService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.query.page.PageQuery;
import com.jovo.gateway.web.util.PmsTreeUtil;
import com.jovo.gateway.web.vo.role.PmsTreeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Api(tags = "权限信息管理")
@RestController
@RequestMapping("/admin/pmss")
@Transactional(rollbackFor = Exception.class)
public class PmsPermissionController extends BaseController {

    @Reference
    private PmsPermissionService pmsPermissionService;

    @Resource
    private PmsTreeUtil pmsTreeUtil;

    @GetMapping("/{pmsId}")
    @ResponseBody
    @ApiOperation(value = "获取权限信息")
    public Object getPermission(@PathVariable("pmsId") Integer pmsId) {
        PmsPermission pmsPermission = pmsPermissionService.selectPmsPermissionByPmsId(pmsId);
        return new GatewayResult(GatewayResultConstant.SUCCESS, pmsPermission);
    }

    @GetMapping(value = "tree")
    @ResponseBody
    @ApiOperation(value = "获取权限树")
    public String getPmsTree() {
        List<PmsPermission> pmsPermissions = pmsPermissionService.getPmsPermissions();

        List<PmsPermission> pmss = pmsTreeUtil.buildTree(pmsPermissions);
        List<PmsTreeVo> pmsTreeVos = buildPmsTree(pmss);

        return JSON.toJSONString(new GatewayResult(GatewayResultConstant.SUCCESS, pmsTreeVos));
    }

    @Log("查询权限")
    @GetMapping(value = "")
    @ResponseBody
    @ApiOperation(value = "获取所有的权限")
    public Object getPmss(PageQuery pageQuery) {
        List<PmsPermission> pmsPermissions = pmsPermissionService.getPmsPermissions();

        List<PmsPermission> pmss = pmsTreeUtil.buildTree(pmsPermissions);

        return new GatewayResult(GatewayResultConstant.SUCCESS, pmss);
    }

    @Log("添加权限")
    @PostMapping
    @ResponseBody
    @ApiOperation(value = "添加权限")
    public Object add(@Validated @RequestBody PmsPermission pmsPermission) {

        int count = pmsPermissionService.insert(pmsPermission);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "添加成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "添加失败");
    }

    @Log("修改权限")
    @PutMapping
    @ResponseBody
    @ApiOperation(value = "修改权限")
    public Object update(@Validated @RequestBody PmsPermission pmsPermission) {

        int count = pmsPermissionService.update(pmsPermission);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "修改成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "修改失败");
    }

    public List<PmsTreeVo> buildPmsTree(List<PmsPermission> permissions) {
        List<PmsTreeVo> pmss = new LinkedList<>();
        permissions.forEach(pms -> {
            if (pms != null) {
                List<PmsPermission> childList = pms.getChildren();
                PmsTreeVo pmsTreeVo = new PmsTreeVo(pms.getMenuId(), pms.getMenuName());

                if (childList != null && childList.size() != 0) {
                    pmsTreeVo.setChildren(buildPmsTree(childList));
                    // 处理是一级菜单并且没有子菜单的情况
                } else if (pms.getPid() == 0) {
                    List<PmsTreeVo> list1 = new ArrayList<PmsTreeVo>();
                    pmsTreeVo.setChildren(list1);
                }

                pmss.add(pmsTreeVo);
            }
        });
        return pmss;
    }
}
