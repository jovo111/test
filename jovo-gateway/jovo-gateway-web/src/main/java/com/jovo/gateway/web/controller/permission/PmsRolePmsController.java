package com.jovo.gateway.web.controller.permission;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsRolePmsService;
import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.model.PmsRolePms;
import com.jovo.gateway.dao.model.PmsUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@Api(tags="角色权限管理")
@RestController
@RequestMapping("/admin/roles/permission")
public class PmsRolePmsController extends BaseController {

    @Reference
    private PmsRolePmsService pmsRolePmsService;

    @RequestMapping("/{id}")
    @ResponseBody
    @ApiOperation(value = "获取角色列表信息")
    public String index(@PathVariable("id") Integer id){
        PmsRolePms pmsRolePms = pmsRolePmsService.selectPmsMenusById(id);
        return JSON.toJSONString(pmsRolePms);
    }

    @PutMapping(value = "")
    @ResponseBody
    public Object updatePms(@Validated @RequestBody PmsRolePms pmsRolePms){
        int count = pmsRolePmsService.updateByRoleId(pmsRolePms);

        if(count > 0){
            return new GatewayResult(GatewayResultConstant.SUCCESS,"修改成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED,"修改失败");
    }
}
