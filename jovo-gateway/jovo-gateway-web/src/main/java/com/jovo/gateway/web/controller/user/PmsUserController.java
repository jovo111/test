package com.jovo.gateway.web.controller.user;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jovo.common.annotation.Log;
import com.jovo.common.base.BaseController;
import com.jovo.common.util.RequestUtil;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.dao.query.page.PageQuery;
import com.jovo.gateway.web.vo.auth.AuthInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@Slf4j
@Api(tags = "用户信息管理")
@RestController
@RequestMapping("/admin/users")
public class PmsUserController extends BaseController {

    @Reference
    private PmsUserService pmsUserService;

    @Reference
    private PmsRoleUserService pmsRoleUserService;

    @RequiresPermissions("system:user:list")
    @GetMapping("/{userId}")
    @ResponseBody
    @ApiOperation(value = "获取用户信息", notes = "通过用户ID获取用户信息")
    public Object getUser(@PathVariable("userId") Integer uid) {

        PmsUser pmsUser = pmsUserService.selectPmsUserByUserId(uid);
        return new GatewayResult(GatewayResultConstant.SUCCESS, pmsUser);
    }

    @GetMapping("/info")
    @ResponseBody
    @ApiOperation(value = "获取用户信息")
    public Object getInfo() {
        PmsUser pmsUser = (PmsUser) SecurityUtils.getSubject().getPrincipal();
        Set<String> pmsPermissions = pmsRoleUserService.selectPmsPermissionsByUid(pmsUser.getUserId());
        if(pmsUser.getUserName().equals("admin")) pmsPermissions.add("ADMIN");
        return new GatewayResult(GatewayResultConstant.SUCCESS, new AuthInfoVo("", pmsUser, pmsPermissions));
    }

    @Log("查询用户")
    @GetMapping
    @ResponseBody
    @RequiresPermissions("system:user:list")
    @ApiOperation(value = "获取所有用户信息", notes = "获取用户信息")
    public Object index(PageQuery pageQuery) {
        PageInfo<PmsUser> pageInfo = pmsUserService.getUsersByExample(pageQuery);
        return JSONObject.toJSONString(new GatewayResult(GatewayResultConstant.SUCCESS, pageInfo));
    }

    @Log("添加用户")
    @PostMapping
    @ResponseBody
    @RequiresPermissions("system:user:create")
    public Object add(@Validated @RequestBody PmsUser pmsUser, HttpServletRequest request) {

        String ipAddr = RequestUtil.getIpAddr(request);

        String account= ((PmsUser)SecurityUtils.getSubject().getPrincipal()).getUserName();

        pmsUser.setCreateip(ipAddr);
        pmsUser.setLastLoginIp(ipAddr);
        pmsUser.setPassword("160ecce9183eccd247487cade4beff6c");

        PmsRoleUser pmsRoleUser = new PmsRoleUser();
        pmsRoleUser.setCreatetime(pmsUser.getCreatetime());
        pmsRoleUser.setUpdatetime(pmsUser.getCreatetime());
        pmsRoleUser.setCreater(account);
        pmsRoleUser.setEditor(account);
        pmsRoleUser.setRoles(pmsUser.getRoles());

        int count = pmsUserService.insertUserRole(pmsUser,pmsRoleUser);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "添加成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "添加失败");
    }

    @Log("修改用户信息")
    @PutMapping
    @RequiresPermissions("system:user:update")
    @ResponseBody
    public Object update(@Validated @RequestBody PmsUser pmsUser) {

        String account= ((PmsUser)SecurityUtils.getSubject().getPrincipal()).getUserName();

        if(pmsUser.getPassword() != null && !pmsUser.getPassword().isEmpty()){
            pmsUser.setPassword("160ecce9183eccd247487cade4beff6c");
        }

        PmsRoleUser pmsRoleUser = new PmsRoleUser();
        pmsRoleUser.setEditor(account);
        pmsRoleUser.setUpdatetime(pmsUser.getCreatetime());
        pmsRoleUser.setUserId(pmsUser.getUserId());
        pmsRoleUser.setRoles(pmsUser.getRoles());

        int count = pmsRoleUserService.updateByUserId(pmsRoleUser);
        count += pmsUserService.update(pmsUser);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "修改成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "修改失败");
    }

    @Log("删除用户")
    @RequiresPermissions("system:user:delete")
    @DeleteMapping("{userId}")
    @ResponseBody
    public Object del(@PathVariable("userId") Integer userId) {

        int count = pmsUserService.delete(userId);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "删除成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "删除失败");
    }
}
