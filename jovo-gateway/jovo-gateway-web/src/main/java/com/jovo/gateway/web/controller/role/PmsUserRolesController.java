package com.jovo.gateway.web.controller.role;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags="用户角色管理")
@RestController
@RequestMapping("/userRoles")
public class PmsUserRolesController extends BaseController {

    @Reference
    private PmsRoleUserService pmsRoleUserService;

    @RequestMapping("/{id}")
    @ResponseBody
    @ApiOperation(value = "获取用户角色信息")
    public String index(@PathVariable("id") Integer id){
        PmsRoleUser pmsRoleUser = pmsRoleUserService.selectPmsRoleById(id);
        return JSONObject.toJSONString(pmsRoleUser);
    }

    @RequestMapping("")
    @ResponseBody
    public String add(PmsRoleUser pmsRoleUser){
        int count = pmsRoleUserService.insert(pmsRoleUser);

        if(count > 0){
            return "SUCCESS";
        }
        return "FAIL";
    }
}
