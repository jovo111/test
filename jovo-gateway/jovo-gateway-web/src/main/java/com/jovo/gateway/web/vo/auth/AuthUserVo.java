package com.jovo.gateway.web.vo.auth;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName AuthUserDto
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-01 14:34
 * @Version V1.0
 */
@Data
public class AuthUserVo {

    private String username;

    private String password;

    private String code;

    private String uuid;

    private String token;

    private boolean rememberMe;
}
