package com.jovo.gateway.web.controller.log;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jovo.common.annotation.Log;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsOptLogService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsOptLog;
import com.jovo.gateway.dao.query.page.PageQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName LogController
 * @Description TODO
 * @Author Jovo
 * @Date 2019-12-16 16:10
 * @Version V1.0
 */
@Slf4j
@RestController
@RequestMapping("/admin/logs")
@Api(tags = "系统监控:日志管理")
public class LogController extends BaseController {

    @Reference
    private PmsOptLogService pmsOptLogService;

//    @Log("日志查询")
    @GetMapping
    @ResponseBody
    @ApiOperation("日志查询")
    public Object getLogs(PageQuery pageQuery) {
        PageInfo<PmsOptLog> pageInfo = pmsOptLogService.getPmsOptLogs(pageQuery);
        return new GatewayResult(GatewayResultConstant.SUCCESS, pageInfo);
    }
}
