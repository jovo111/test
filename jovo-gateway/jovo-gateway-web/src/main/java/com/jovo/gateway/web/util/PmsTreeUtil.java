package com.jovo.gateway.web.util;

import com.jovo.gateway.dao.model.PmsPermission;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @ClassName PmsTreeUtil
 * @Description 用于处理权限层级无限的问题
 * @Author Jovo
 * @Date 2019-12-11 17:27
 * @Version V1.0
 */
@Component
public class PmsTreeUtil {

    /**
     * @Author Jovo
     * @Description 生成MenuTree,待优化
     * @Date 9:59 2019-12-11
     * @Param [menus]
     * @return java.lang.Object
     **/
    public List<PmsPermission> buildTree(List<PmsPermission> menus) {

        List<PmsPermission> trees = new CopyOnWriteArrayList<PmsPermission>();
        for (PmsPermission menu : menus) {
            if (menu.getPid() == 0) {
                menu.setChildren(getChild(menus,menu.getMenuId()));
                trees.add(menu);
            }
        }

        return trees;
    }

    public List<PmsPermission> getChild(List<PmsPermission> trees,Integer pid){
        List<PmsPermission> childs = new ArrayList<PmsPermission>();
        for (PmsPermission tree : trees) {
            if(tree.getPid().equals(pid)){
                tree.setChildren(getChild(trees,tree.getMenuId()));
                childs.add(tree);
            }
        }

        return childs;
    }
}
