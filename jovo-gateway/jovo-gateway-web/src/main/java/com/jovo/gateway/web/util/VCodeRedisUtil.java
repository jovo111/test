package com.jovo.gateway.web.util;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName vCodeRedisUtil
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-30 11:15
 * @Version V1.0
 */
@Slf4j
@Component
public class VCodeRedisUtil {

    //设置验证码失效时间
    @Getter
    public static int MAX_AGE;

    //设置验证码前缀
    @Getter
    public static String PREFIX;


    @Value("${vCode.maxAge}")
    public void setMaxAge(int maxAge) {
        this.MAX_AGE = maxAge;
    }

    @Value("${vCode.prefix}")
    public void setPrefix(String prefix) {
        this.PREFIX = prefix;
    }
}
