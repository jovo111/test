package com.jovo.gateway.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName ImgResult
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-30 10:42
 * @Version V1.0
 */

@Data
@AllArgsConstructor
public class ImgResult {

    private String img;

    private String uuid;
}
