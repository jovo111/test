package com.jovo.gateway.web.util;

import com.jovo.common.base.BaseRedis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName RedisUtil
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-11 17:49
 * @Version V1.0
 */
@Slf4j
@Component
public class RedisUtil implements BaseRedis {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public RedisUtil(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * @return java.lang.Object
     * @Author Jovo
     * @Description 获取一条数据
     * @Date 19:47 2019-10-15
     * @Param [key]
     **/
    @Override
    public Object get(Object key) {

        try {
            return redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * @return void
     * @Author Jovo
     * @Description 插入一条数据
     * @Date 19:45 2019-10-15
     * @Param [key, value]
     **/
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * @Author Jovo
     * @Description 插入有时间的key
     * @Date 14:58 2019-10-31
     * @Param [key, value, seconds]
     * @return void
     **/
    public void set(String key, Object value, long seconds) {
        redisTemplate.opsForValue().set(key, value, seconds, TimeUnit.SECONDS);
    }

    /**
     * @return void
     * @Author Jovo
     * @Description 删除一条数据
     * @Date 19:45 2019-10-15
     * @Param [key]
     **/
    public void del(String key) {
        redisTemplate.delete(key);
    }

    public void expire(String key, long seconds) {
        redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
    }

    /**
     * @Author Jovo
     * @Description 根据key的前缀，获取指定的key
     * @Date 10:42 2019-10-15
     * @Param [keyPrefix]
     * @return java.util.Set<byte[]>
     **/
    public Set<String> keys(String keyPrefix) {
        keyPrefix = keyPrefix + "*";
        return redisTemplate.keys(keyPrefix);
    }

    /**
     * @Author Jovo
     * @Description 根据key的前缀，批量删除key
     * @Date 10:41 2019-10-15
     * @Param [keyPrefix]
     * @return void
     **/
    public void delByPrefix(String keyPrefix) {
        keyPrefix = keyPrefix + "*";

        Set<String> keyArraySet = redisTemplate.keys(keyPrefix);
        for(String key : keyArraySet){
            redisTemplate.delete(key);
        }
    }
}
