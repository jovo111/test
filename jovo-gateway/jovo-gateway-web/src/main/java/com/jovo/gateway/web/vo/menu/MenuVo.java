package com.jovo.gateway.web.vo.menu;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName MenuVo
 * @Description TODO
 * @Author Jovo
 * @Date 2019-12-07 17:10
 * @Version V1.0
 */
@Data
public class MenuVo implements Serializable {

    private String name;

    private String path;

    private Boolean hidden;

    private String redirect;

    private String component;

    private Boolean alwaysShow;

    private MenuMetaVo meta;

    private List<MenuVo> children;
}
