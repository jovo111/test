package com.jovo.gateway.web.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @ClassName SpringBeanFactoryUtils
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-05 11:26
 * @Version V1.0
 */
@Slf4j
@Component
public class SpringBeanFactoryUtil implements ApplicationContextAware {

    private static ApplicationContext context = null;

    public static <T> T getBean(String name, Class<T> type) {
        return context.getBean(name, type);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringBeanFactoryUtil.context == null) {
            SpringBeanFactoryUtil.context = applicationContext;
        }
    }
}
