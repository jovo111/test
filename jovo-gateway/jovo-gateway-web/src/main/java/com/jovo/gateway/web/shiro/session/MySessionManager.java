package com.jovo.gateway.web.shiro.session;

import com.jovo.gateway.web.config.ShiroConfiguration;
import com.jovo.gateway.web.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.eclipse.jetty.util.StringUtil;
import org.thymeleaf.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;

/**
 * @ClassName MySessionManager
 * @Description 处理前后端sessionID 共享问题
 * @Author Jovo
 * @Date 2019-11-28 10:52
 * @Version V1.0
 */
@Slf4j
public class MySessionManager extends DefaultWebSessionManager {

    private RedisUtil redisUtil = ShiroConfiguration.redisUtil;

    private static final String AUTHORIZATION = "Authorization";

    public MySessionManager() {
        super();
    }

    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
        String token = WebUtils.toHttp(request).getHeader(AUTHORIZATION);

        if (!StringUtil.isEmpty(token)) {
            String key = "jovo-gateway-web-session-id:" + token.split(":")[1];
            if(redisUtil.get(key) == null){
                throw new AuthenticationException();
            }
            String sessionId = redisUtil.get(key).toString();
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, "Stateless request");
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, sessionId);
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);

            return sessionId;
        } else {
            return super.getSessionId(request, response);
        }
    }


}
