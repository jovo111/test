package com.jovo.gateway.web.vo.menu;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName MenuMetaVo
 * @Description TODO
 * @Author Jovo
 * @Date 2019-12-07 17:12
 * @Version V1.0
 */
@Data
@AllArgsConstructor
public class MenuMetaVo {

    private String title;

    private String icon;

    private Boolean noCache;

    public MenuMetaVo() {
    }
}
