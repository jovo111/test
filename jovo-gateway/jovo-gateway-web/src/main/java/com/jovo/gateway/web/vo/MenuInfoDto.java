package com.jovo.gateway.web.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.*;

/**
 * @ClassName MenuInfoDto
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-15 16:32
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public class MenuInfoDto {

    private String name;
    private String path;
    private String redirect;
    private boolean hidden;
    private String component;
    private boolean alwaysShow;
    private List children;
}
