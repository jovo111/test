package com.jovo.gateway.web.shiro.cache;

import com.alibaba.fastjson.JSON;
import com.jovo.common.util.SecurityUtil;
import com.jovo.gateway.web.config.ShiroConfiguration;
import com.jovo.gateway.web.util.RedisUtil;
import com.jovo.gateway.web.util.SerializableUtil;
import com.jovo.gateway.web.util.ShiroUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @ClassName RedisShiroCache
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-11 10:13
 * @Version V1.0
 */
@Slf4j
public class SsoShiroCache<K, V> implements Cache<K, V> {

    private RedisUtil redisUtil = ShiroConfiguration.redisUtil;

    /**
     * @return byte[]
     * @Author Jovo
     * @Description 获取cache的前缀
     * @Date 16:42 2019-10-12
     * @Param [key]
     **/
    private String getKey(Object key) {
        return ShiroUtil.SHIRO_CACHE_PREFIX + key;
    }

    /**
     * @return java.lang.Object
     * @Author Jovo
     * @Description
     * @Date 16:43 2019-10-12
     * @Param [key]
     **/
    @Override
    public Object get(Object key) throws CacheException {
        if (key == null) {
            return null;
        }
        String k = getKey(key.toString());
        // 判断key 是否是授权使用
        if (key instanceof PrincipalCollection){
            k = getKey(SecurityUtil.getUserName());
        }
        // 判断value 是否为空
        if (redisUtil.get(k) == null) {
            return null;
        }
        // 从redis中获取数据
        Object value = SerializableUtil.deserialize(redisUtil.get(k).toString());
        log.debug("获取缓存key:{},value:{}", k, JSON.toJSONString(value));
        // 返回对应的数据
        return value;
    }

    /**
     * @return V
     * @Author Jovo
     * @Description 保存shiro到redis
     * @Date 16:46 2019-10-12
     * @Param [k, v]
     **/
    @Override
    public Object put(Object key, Object value) throws CacheException {
        if (key == null || value == null) {
            return null;
        }
        String k = getKey(key.toString());
        if (key instanceof PrincipalCollection){
            k = getKey(SecurityUtil.getUserName());
        }

        redisUtil.set(k, SerializableUtil.serialize(value), ShiroUtil.EXPIRE_SECONDS);
        log.debug("更新缓存key:{}", key.toString());
        // 返回保存的值
        return value;
    }

    /**
     * @return java.lang.Object
     * @Author Jovo
     * @Description 删除指定缓存
     * @Date 16:56 2019-10-12
     * @Param [key]
     **/
    @Override
    public Object remove(Object key) throws CacheException {
        if (key == null) {
            return null;
        }
        String k = getKey(key);
        // 判断key 是否是授权使用
        if (key instanceof PrincipalCollection){
            k = getKey(SecurityUtil.getUserName());
        }

        if (redisUtil.get(k) == null) {
            return null;
        }
        Object value = SerializableUtil.deserialize(redisUtil.get(k).toString());
        // 删除
        redisUtil.del(k);
        log.debug("从redis中删除");
        // 返回删除的数据
        return value;
    }

    /**
     * @return void
     * @Author Jovo
     * @Description 清空所有缓存
     * @Date 16:57 2019-10-12
     * @Param []
     **/
    @Override
    public void clear() throws CacheException {
        log.debug("清空所有的缓存");
        redisUtil.delByPrefix(ShiroUtil.SHIRO_CACHE_PREFIX);
    }

    /**
     * @return int
     * @Author Jovo
     * @Description 缓存个数
     * @Date 16:57 2019-10-12
     * @Param []
     **/
    @Override
    public int size() {
        Set<String> keyByteArraySet = redisUtil.keys(ShiroUtil.SHIRO_CACHE_PREFIX);
        log.debug("获取缓存个数");
        return keyByteArraySet.size();
    }

    /**
     * @return java.util.Set<K>
     * @Author Jovo
     * @Description 获取所有的key
     * @Date 11:45 2019-10-11
     * @Param []
     **/
    @Override
    public Set keys() {
        Set<String> keys = redisUtil.keys(ShiroUtil.SHIRO_CACHE_PREFIX);
        log.debug("获取缓存所有的key");
        return keys;
    }

    /**
     * @return java.util.Collection<V>
     * @Author Jovo
     * @Description 获取所有的value
     * @Date 16:59 2019-10-12
     * @Param []
     **/
    @Override
    public Collection values() {
        Set keySet = this.keys();
        List<Object> valueList = new ArrayList<>(16);
        for (Object key : keySet) {
            Object object = SerializableUtil.deserialize(redisUtil.get(key).toString());
            valueList.add(object);
        }
        log.debug("获取缓存所有的value");
        return valueList;
    }
}
