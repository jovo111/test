package com.jovo.gateway.web.shiro.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName SsoAuthenicationFilter
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-12 10:56
 * @Version V1.0
 */
@Slf4j
public class SsoAuthenticationFilter extends AuthenticationFilter {



    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        HttpServletRequest req = (HttpServletRequest) request;
        String authorization = req.getHeader("Authorization");
        log.info("----------------------Authorization:{}",authorization);

        Subject subject = getSubject(request,response);
        Session session = subject.getSession();

        return subject.isAuthenticated();
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        WebUtils.toHttp(response).sendRedirect("http://localhost:8013/login");
        return false;
    }
}
