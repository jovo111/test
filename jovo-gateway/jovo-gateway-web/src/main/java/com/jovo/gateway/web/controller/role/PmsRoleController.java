package com.jovo.gateway.web.controller.role;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.jovo.common.annotation.Log;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsRoleService;
import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.common.GatewayResult;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.dao.query.page.PageQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "角色信息管理")
@RestController
@RequestMapping("/admin/roles")
@Transactional(rollbackFor = Exception.class)
public class PmsRoleController extends BaseController {

    @Reference
    private PmsRoleService pmsRoleService;

    @Log("角色信息查询")
    @GetMapping("/{roleId}")
    @ResponseBody
    @ApiOperation(value = "获取角色信息", notes = "通过用户ID获取用户信息")
    public String index(@PathVariable("roleId") Integer roleId) {
        PmsRole pmsRole = pmsRoleService.selectPmsRoleByRoleId(roleId);
        return JSONObject.toJSONString(pmsRole);
    }

    @Log("角色列表")
    @GetMapping
    @ResponseBody
    @ApiOperation(value = "获取所有的角色")
    public Object getRoles(PageQuery pageQuery) {
        PageInfo<PmsRole> pageInfo = pmsRoleService.getPmsRoles(pageQuery);
        return JSON.toJSONString(new GatewayResult(GatewayResultConstant.SUCCESS, pageInfo));
    }

    @GetMapping(value = "build")
    @ResponseBody
    @ApiOperation(value = "用于角色选择器使用")
    public Object getRoleBySelect() {
        List<PmsRole> roles = pmsRoleService.getPmsRolesBySelect();
        return JSON.toJSONString(new GatewayResult(GatewayResultConstant.SUCCESS, roles));
    }

    @Log("添加角色")
    @PostMapping
    @ResponseBody
    @ApiOperation(value = "添加角色")
    public Object add(@Validated @RequestBody PmsRole pmsRole) {
        Long now = System.currentTimeMillis() / 1000;
        pmsRole.setCreatetime(now);
        pmsRole.setUpdatetime(now);
        int count = pmsRoleService.insert(pmsRole);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "添加成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "添加失败");
    }

    @Log("修改角色")
    @PutMapping
    @ResponseBody
    @ApiOperation(value = "修改角色信息")
    public Object update(@Validated @RequestBody PmsRole pmsRole) {
        Long now = System.currentTimeMillis() / 1000;

        pmsRole.setUpdatetime(now);
        int count = pmsRoleService.update(pmsRole);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "修改成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "修改失败");
    }

    @DeleteMapping(value = "{roleId}")
    @ResponseBody
    @ApiOperation(value = "删除角色")
    public Object add(@PathVariable("roleId") Integer roleId) {
        int count = pmsRoleService.delete(roleId);

        if (count > 0) {
            return new GatewayResult(GatewayResultConstant.SUCCESS, "删除成功");
        }
        return new GatewayResult(GatewayResultConstant.FAILED, "删除失败");
    }
}
