package com.jovo.gateway.web.shiro.session;

import com.jovo.gateway.web.config.ShiroConfiguration;
import com.jovo.gateway.web.util.RedisUtil;
import com.jovo.gateway.web.util.SerializableUtil;
import com.jovo.gateway.web.util.ShiroUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.ValidatingSession;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;

/**
 * @ClassName RedisShiroSessionDao
 * @Description 使用redis管理shiro session
 * @Author Jovo
 * @Date 2019-10-11 11:27
 * @Version V1.0
 */
@Slf4j
@Component
public class SsoSessionDao extends CachingSessionDAO {

    private RedisUtil redisUtil = ShiroConfiguration.redisUtil;

    private String getKey(String key) {
        return ShiroUtil.SHIRO_SESSION_PREFIX + key;
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = generateSessionId(session);
        assignSessionId(session, sessionId);
        redisUtil.set(getKey(sessionId.toString()), SerializableUtil.serialize(session), ShiroUtil.EXPIRE_SECONDS);
        log.debug("doCreate >>>>> sessionId={}", sessionId);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        String session = (String) redisUtil.get(getKey(sessionId.toString()));
        log.debug("doReadSession >>>>> sessionId={}", sessionId);
        return (Session)SerializableUtil.deserialize(session);
    }

    @Override
    protected void doUpdate(Session session) {
        // 如果会话过期/停止 没必要再更新了
        if (session instanceof ValidatingSession && !((ValidatingSession) session).isValid()) {
            return;
        }
        redisUtil.set(getKey(session.getId().toString()), SerializableUtil.serialize(session), ShiroUtil.EXPIRE_SECONDS);

        log.debug("doUpdate >>>>> sessionId={}", session.getId());
    }

    @Override
    protected void doDelete(Session session) {
        String sessionId = session.getId().toString();
        String code = (String) redisUtil.get(sessionId.toString());
        // 清除全局会话
        redisUtil.del(getKey(sessionId.toString()));
        log.debug("doUpdate >>>>> sessionId={}", sessionId);
    }

    /**
     * @return java.util.Collection<org.apache.shiro.session.Session>
     * @Author Jovo
     * @Description
     * @Date 17:04 2019-11-06
     * @Param []
     **/
    @Override
    public Collection<Session> getActiveSessions() {

        List<Session> sessionList = new ArrayList<>(16);
        // 从redis从查询
        Set<String> keyArraySet = redisUtil.keys(ShiroUtil.SHIRO_SESSION_PREFIX);
        for (String key : keyArraySet) {
            // 反序列化
            String sessionSer = (String)redisUtil.get(key);
            Session session = (Session) SerializableUtil.deserialize(sessionSer);
            sessionList.add(session);
        }
        return sessionList;
    }
}
