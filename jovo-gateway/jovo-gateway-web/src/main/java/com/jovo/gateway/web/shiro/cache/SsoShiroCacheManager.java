package com.jovo.gateway.web.shiro.cache;

import com.jovo.gateway.web.shiro.cache.SsoShiroCache;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * @ClassName RedisShiroCacheManager
 * @Description shiro redis 缓存管理器
 * @Author Jovo
 * @Date 2019-10-11 10:11
 * @Version V1.0
 */
public class SsoShiroCacheManager implements CacheManager {
    
    /**
     * @Author Jovo
     * @Description
     * @Date 10:12 2019-10-11
     * @Param [s]
     * @return org.apache.shiro.cache.Cache<K,V>
     **/
    @Override
    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
        return new SsoShiroCache<K,V>();
    }
}
