package com.jovo.gateway.web.shiro.session;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionFactory;
import org.apache.shiro.web.session.mgt.WebSessionContext;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName SsoSessionFactory
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-09 15:12
 * @Version V1.0
 */
public class SsoSessionFactory implements SessionFactory {

    @Override
    public Session createSession(SessionContext sessionContext) {
        SsoSession ssoSession = new SsoSession();
        if (null != sessionContext && sessionContext instanceof WebSessionContext) {
            WebSessionContext webSessionContext = (WebSessionContext) sessionContext;
            HttpServletRequest request = (HttpServletRequest) webSessionContext.getServletRequest();
            if (null != request) {
                ssoSession.setHost(request.getRemoteAddr());
                ssoSession.setUserAgent(request.getHeader("User-Agent"));
            }
        }
        return ssoSession;
    }
}
