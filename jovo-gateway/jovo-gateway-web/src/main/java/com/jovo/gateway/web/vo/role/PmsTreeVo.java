package com.jovo.gateway.web.vo.role;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @ClassName PmsTreeVo
 * @Description TODO
 * @Author Jovo
 * @Date 2019-12-11 17:53
 * @Version V1.0
 */
@Data
public class PmsTreeVo {

    private Integer id;

    private String label;

    private List<PmsTreeVo> children;

    public PmsTreeVo(Integer id, String label) {
        this.id = id;
        this.label = label;
    }

    public PmsTreeVo() {
    }
}
