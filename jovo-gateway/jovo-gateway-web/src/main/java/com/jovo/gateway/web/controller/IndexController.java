package com.jovo.gateway.web.controller;

import com.jovo.common.base.BaseController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class IndexController extends BaseController {

    @GetMapping("")
    @ResponseBody
    public String index(){
        return "GateWay";
    }
}
