package com.jovo.gateway.web.controller.menu;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jovo.common.base.BaseController;
import com.jovo.gateway.api.PmsRoleMenuService;
import com.jovo.gateway.common.GatewayResultConstant;
import com.jovo.gateway.dao.model.PmsRoleMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName PmsRoleMenuController
 * @Description TODO
 * @Author Jovo
 * @Date 2019-11-29 10:12
 * @Version V1.0
 */
@Api(tags = "角色目录管理")
@Controller
@RequestMapping("/roleMenu")
@Transactional(rollbackFor = Exception.class)
public class PmsRoleMenuController extends BaseController {

    @Reference
    private PmsRoleMenuService pmsRoleMenuService;

    @CrossOrigin
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "获取所有目录列表", notes = "获取所有目录")
    public Object getMenus() {
        List<PmsRoleMenu> pmsRoleMenus = pmsRoleMenuService.getPmsRoleMenus();

        return pmsRoleMenus;
    }

    @CrossOrigin
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "添加目录", notes = "添加目录")
    public Object add(PmsRoleMenu pmsRoleMenu) {
        long now = System.currentTimeMillis() / 1000;
        pmsRoleMenu.setCreatetime(now);
        pmsRoleMenu.setUpdatetime(now);
        int count = pmsRoleMenuService.insert(pmsRoleMenu);

        if (count > 0) {
            return "SUCCESS";
        }

        return GatewayResultConstant.FAILED;
    }


    @CrossOrigin
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @ApiOperation(value = "删除单个目录", notes = "删除")
    public Object del(@PathVariable("id") Integer id) {
        int count = pmsRoleMenuService.delete(id);

        if (count > 0) {
            return "SUCCESS";
        }

        return GatewayResultConstant.FAILED;
    }

    @CrossOrigin
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "根据id 查询单个目录信息", notes = "查询")
    public Object getMenu(@PathVariable("id") Integer id) {
        PmsRoleMenu pmsRoleMenu = pmsRoleMenuService.selectPmsRoleMenuById(id);

        return pmsRoleMenu;
    }

    @CrossOrigin
    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    @ApiOperation(value = "根据id 修改单个目录信息", notes = "修改")
    public Object update(PmsRoleMenu pmsRoleMenu) {
        long now = System.currentTimeMillis() / 1000;
        pmsRoleMenu.setUpdatetime(now);

        int count = pmsRoleMenuService.update(pmsRoleMenu);

        if (count > 0) {
            return GatewayResultConstant.SUCCESS;
        }

        return GatewayResultConstant.FAILED;
    }
}
