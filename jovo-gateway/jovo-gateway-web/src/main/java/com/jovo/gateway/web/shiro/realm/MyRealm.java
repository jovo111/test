package com.jovo.gateway.web.shiro.realm;

import com.alibaba.fastjson.JSON;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.web.util.SpringBeanFactoryUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;

import java.util.Set;

@Slf4j
public class MyRealm extends AuthorizingRealm {

    /**
     * 授权
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        log.debug("用户请求授权");

        PmsUser pmsUser = (PmsUser) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();

        PmsRoleUserService pmsRoleUserService = SpringBeanFactoryUtil.getBean("pmsRoleUserService",PmsRoleUserService.class);

        Set<String> roles = pmsRoleUserService.selectPmsRolesByUid(pmsUser.getUserId());
        Set<String> pmss = pmsRoleUserService.selectPmsPermissionsByUid(pmsUser.getUserId());

        // 当前用户拥有的角色
        simpleAuthorizationInfo.setRoles(roles);
        // 当前用户拥有的权限
        simpleAuthorizationInfo.setStringPermissions(pmss);

        return simpleAuthorizationInfo;
    }

    /**
     * 认证
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username = (String) token.getPrincipal();

        PmsUserService pmsUserService = SpringBeanFactoryUtil.getBean("pmsUserService",PmsUserService.class);
        PmsUser pmsUser = pmsUserService.selectPmsUserByUserName(username);

        if (null == pmsUser) {
            throw new UnknownAccountException();
        }
        if (!pmsUser.getStatus()) {
            throw new LockedAccountException();
        }

        String salt = "qg482pbznx94b5sx";

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                pmsUser,
                pmsUser.getPassword(),
                ByteSource.Util.bytes(salt),
                this.getName());

        return authenticationInfo;
    }
}
