package com.jovo.gateway.web;

import com.jovo.gateway.web.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @ClassName RedisTests
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-15 11:10
 * @Version V1.0
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTests {

    @Resource
    private RedisUtil redisUtil;

    @Test
    public void insert(){
        String key = "key";
        String value = "value";
        redisUtil.set(key,value);
        redisUtil.expire(key,100000);
    }

    @Test
    public void remove(){
        String key = "key";
        redisUtil.del(key);
    }

    @Test
    public void setList(){
        String keyPrefix = "test:";
        String valuePrefix = "value:";
        Long now = System.currentTimeMillis();
        log.info("----------------------begin:{}",now);
        for(int i=0;i<100000;i++){
            redisUtil.set(keyPrefix+i,valuePrefix+i);
//            redisUtil.expire(keyPrefix+i,1000000);
        }
        log.info("----------------------end:{}",System.currentTimeMillis()-now);
    }

    @Test
    public void delPrefix() {
        String keyPrefix = "test:";
        redisUtil.delByPrefix(keyPrefix);
    }
}
