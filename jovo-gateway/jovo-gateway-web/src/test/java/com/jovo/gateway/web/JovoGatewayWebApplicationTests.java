package com.jovo.gateway.web;

import com.jovo.common.util.RequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class JovoGatewayWebApplicationTests {

    @Test
    public void contextLoads() {
        String ip = "220.248.12.158";
        log.info(RequestUtil.getAddress(ip));
    }

}
