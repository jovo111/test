package com.jovo.gateway.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PmsOptLogExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    public PmsOptLogExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLogIdIsNull() {
            addCriterion("log_id is null");
            return (Criteria) this;
        }

        public Criteria andLogIdIsNotNull() {
            addCriterion("log_id is not null");
            return (Criteria) this;
        }

        public Criteria andLogIdEqualTo(Integer value) {
            addCriterion("log_id =", value, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdNotEqualTo(Integer value) {
            addCriterion("log_id <>", value, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdGreaterThan(Integer value) {
            addCriterion("log_id >", value, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("log_id >=", value, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdLessThan(Integer value) {
            addCriterion("log_id <", value, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdLessThanOrEqualTo(Integer value) {
            addCriterion("log_id <=", value, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdIn(List<Integer> values) {
            addCriterion("log_id in", values, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdNotIn(List<Integer> values) {
            addCriterion("log_id not in", values, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdBetween(Integer value1, Integer value2) {
            addCriterion("log_id between", value1, value2, "logId");
            return (Criteria) this;
        }

        public Criteria andLogIdNotBetween(Integer value1, Integer value2) {
            addCriterion("log_id not between", value1, value2, "logId");
            return (Criteria) this;
        }

        public Criteria andLogTypeIsNull() {
            addCriterion("log_type is null");
            return (Criteria) this;
        }

        public Criteria andLogTypeIsNotNull() {
            addCriterion("log_type is not null");
            return (Criteria) this;
        }

        public Criteria andLogTypeEqualTo(String value) {
            addCriterion("log_type =", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeNotEqualTo(String value) {
            addCriterion("log_type <>", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeGreaterThan(String value) {
            addCriterion("log_type >", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeGreaterThanOrEqualTo(String value) {
            addCriterion("log_type >=", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeLessThan(String value) {
            addCriterion("log_type <", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeLessThanOrEqualTo(String value) {
            addCriterion("log_type <=", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeLike(String value) {
            addCriterion("log_type like", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeNotLike(String value) {
            addCriterion("log_type not like", value, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeIn(List<String> values) {
            addCriterion("log_type in", values, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeNotIn(List<String> values) {
            addCriterion("log_type not in", values, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeBetween(String value1, String value2) {
            addCriterion("log_type between", value1, value2, "logType");
            return (Criteria) this;
        }

        public Criteria andLogTypeNotBetween(String value1, String value2) {
            addCriterion("log_type not between", value1, value2, "logType");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andOptIpIsNull() {
            addCriterion("opt_ip is null");
            return (Criteria) this;
        }

        public Criteria andOptIpIsNotNull() {
            addCriterion("opt_ip is not null");
            return (Criteria) this;
        }

        public Criteria andOptIpEqualTo(String value) {
            addCriterion("opt_ip =", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpNotEqualTo(String value) {
            addCriterion("opt_ip <>", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpGreaterThan(String value) {
            addCriterion("opt_ip >", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpGreaterThanOrEqualTo(String value) {
            addCriterion("opt_ip >=", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpLessThan(String value) {
            addCriterion("opt_ip <", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpLessThanOrEqualTo(String value) {
            addCriterion("opt_ip <=", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpLike(String value) {
            addCriterion("opt_ip like", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpNotLike(String value) {
            addCriterion("opt_ip not like", value, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpIn(List<String> values) {
            addCriterion("opt_ip in", values, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpNotIn(List<String> values) {
            addCriterion("opt_ip not in", values, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpBetween(String value1, String value2) {
            addCriterion("opt_ip between", value1, value2, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptIpNotBetween(String value1, String value2) {
            addCriterion("opt_ip not between", value1, value2, "optIp");
            return (Criteria) this;
        }

        public Criteria andOptAddressIsNull() {
            addCriterion("opt_address is null");
            return (Criteria) this;
        }

        public Criteria andOptAddressIsNotNull() {
            addCriterion("opt_address is not null");
            return (Criteria) this;
        }

        public Criteria andOptAddressEqualTo(String value) {
            addCriterion("opt_address =", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressNotEqualTo(String value) {
            addCriterion("opt_address <>", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressGreaterThan(String value) {
            addCriterion("opt_address >", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressGreaterThanOrEqualTo(String value) {
            addCriterion("opt_address >=", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressLessThan(String value) {
            addCriterion("opt_address <", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressLessThanOrEqualTo(String value) {
            addCriterion("opt_address <=", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressLike(String value) {
            addCriterion("opt_address like", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressNotLike(String value) {
            addCriterion("opt_address not like", value, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressIn(List<String> values) {
            addCriterion("opt_address in", values, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressNotIn(List<String> values) {
            addCriterion("opt_address not in", values, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressBetween(String value1, String value2) {
            addCriterion("opt_address between", value1, value2, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptAddressNotBetween(String value1, String value2) {
            addCriterion("opt_address not between", value1, value2, "optAddress");
            return (Criteria) this;
        }

        public Criteria andOptBrowserIsNull() {
            addCriterion("opt_browser is null");
            return (Criteria) this;
        }

        public Criteria andOptBrowserIsNotNull() {
            addCriterion("opt_browser is not null");
            return (Criteria) this;
        }

        public Criteria andOptBrowserEqualTo(String value) {
            addCriterion("opt_browser =", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserNotEqualTo(String value) {
            addCriterion("opt_browser <>", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserGreaterThan(String value) {
            addCriterion("opt_browser >", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserGreaterThanOrEqualTo(String value) {
            addCriterion("opt_browser >=", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserLessThan(String value) {
            addCriterion("opt_browser <", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserLessThanOrEqualTo(String value) {
            addCriterion("opt_browser <=", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserLike(String value) {
            addCriterion("opt_browser like", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserNotLike(String value) {
            addCriterion("opt_browser not like", value, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserIn(List<String> values) {
            addCriterion("opt_browser in", values, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserNotIn(List<String> values) {
            addCriterion("opt_browser not in", values, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserBetween(String value1, String value2) {
            addCriterion("opt_browser between", value1, value2, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptBrowserNotBetween(String value1, String value2) {
            addCriterion("opt_browser not between", value1, value2, "optBrowser");
            return (Criteria) this;
        }

        public Criteria andOptOsIsNull() {
            addCriterion("opt_os is null");
            return (Criteria) this;
        }

        public Criteria andOptOsIsNotNull() {
            addCriterion("opt_os is not null");
            return (Criteria) this;
        }

        public Criteria andOptOsEqualTo(String value) {
            addCriterion("opt_os =", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsNotEqualTo(String value) {
            addCriterion("opt_os <>", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsGreaterThan(String value) {
            addCriterion("opt_os >", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsGreaterThanOrEqualTo(String value) {
            addCriterion("opt_os >=", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsLessThan(String value) {
            addCriterion("opt_os <", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsLessThanOrEqualTo(String value) {
            addCriterion("opt_os <=", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsLike(String value) {
            addCriterion("opt_os like", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsNotLike(String value) {
            addCriterion("opt_os not like", value, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsIn(List<String> values) {
            addCriterion("opt_os in", values, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsNotIn(List<String> values) {
            addCriterion("opt_os not in", values, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsBetween(String value1, String value2) {
            addCriterion("opt_os between", value1, value2, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptOsNotBetween(String value1, String value2) {
            addCriterion("opt_os not between", value1, value2, "optOs");
            return (Criteria) this;
        }

        public Criteria andOptMethodIsNull() {
            addCriterion("opt_method is null");
            return (Criteria) this;
        }

        public Criteria andOptMethodIsNotNull() {
            addCriterion("opt_method is not null");
            return (Criteria) this;
        }

        public Criteria andOptMethodEqualTo(String value) {
            addCriterion("opt_method =", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodNotEqualTo(String value) {
            addCriterion("opt_method <>", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodGreaterThan(String value) {
            addCriterion("opt_method >", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodGreaterThanOrEqualTo(String value) {
            addCriterion("opt_method >=", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodLessThan(String value) {
            addCriterion("opt_method <", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodLessThanOrEqualTo(String value) {
            addCriterion("opt_method <=", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodLike(String value) {
            addCriterion("opt_method like", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodNotLike(String value) {
            addCriterion("opt_method not like", value, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodIn(List<String> values) {
            addCriterion("opt_method in", values, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodNotIn(List<String> values) {
            addCriterion("opt_method not in", values, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodBetween(String value1, String value2) {
            addCriterion("opt_method between", value1, value2, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptMethodNotBetween(String value1, String value2) {
            addCriterion("opt_method not between", value1, value2, "optMethod");
            return (Criteria) this;
        }

        public Criteria andOptParamsIsNull() {
            addCriterion("opt_params is null");
            return (Criteria) this;
        }

        public Criteria andOptParamsIsNotNull() {
            addCriterion("opt_params is not null");
            return (Criteria) this;
        }

        public Criteria andOptParamsEqualTo(String value) {
            addCriterion("opt_params =", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsNotEqualTo(String value) {
            addCriterion("opt_params <>", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsGreaterThan(String value) {
            addCriterion("opt_params >", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsGreaterThanOrEqualTo(String value) {
            addCriterion("opt_params >=", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsLessThan(String value) {
            addCriterion("opt_params <", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsLessThanOrEqualTo(String value) {
            addCriterion("opt_params <=", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsLike(String value) {
            addCriterion("opt_params like", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsNotLike(String value) {
            addCriterion("opt_params not like", value, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsIn(List<String> values) {
            addCriterion("opt_params in", values, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsNotIn(List<String> values) {
            addCriterion("opt_params not in", values, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsBetween(String value1, String value2) {
            addCriterion("opt_params between", value1, value2, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptParamsNotBetween(String value1, String value2) {
            addCriterion("opt_params not between", value1, value2, "optParams");
            return (Criteria) this;
        }

        public Criteria andOptExceptionIsNull() {
            addCriterion("opt_exception is null");
            return (Criteria) this;
        }

        public Criteria andOptExceptionIsNotNull() {
            addCriterion("opt_exception is not null");
            return (Criteria) this;
        }

        public Criteria andOptExceptionEqualTo(String value) {
            addCriterion("opt_exception =", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionNotEqualTo(String value) {
            addCriterion("opt_exception <>", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionGreaterThan(String value) {
            addCriterion("opt_exception >", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionGreaterThanOrEqualTo(String value) {
            addCriterion("opt_exception >=", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionLessThan(String value) {
            addCriterion("opt_exception <", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionLessThanOrEqualTo(String value) {
            addCriterion("opt_exception <=", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionLike(String value) {
            addCriterion("opt_exception like", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionNotLike(String value) {
            addCriterion("opt_exception not like", value, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionIn(List<String> values) {
            addCriterion("opt_exception in", values, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionNotIn(List<String> values) {
            addCriterion("opt_exception not in", values, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionBetween(String value1, String value2) {
            addCriterion("opt_exception between", value1, value2, "optException");
            return (Criteria) this;
        }

        public Criteria andOptExceptionNotBetween(String value1, String value2) {
            addCriterion("opt_exception not between", value1, value2, "optException");
            return (Criteria) this;
        }

        public Criteria andOptInfoIsNull() {
            addCriterion("opt_info is null");
            return (Criteria) this;
        }

        public Criteria andOptInfoIsNotNull() {
            addCriterion("opt_info is not null");
            return (Criteria) this;
        }

        public Criteria andOptInfoEqualTo(String value) {
            addCriterion("opt_info =", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoNotEqualTo(String value) {
            addCriterion("opt_info <>", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoGreaterThan(String value) {
            addCriterion("opt_info >", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoGreaterThanOrEqualTo(String value) {
            addCriterion("opt_info >=", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoLessThan(String value) {
            addCriterion("opt_info <", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoLessThanOrEqualTo(String value) {
            addCriterion("opt_info <=", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoLike(String value) {
            addCriterion("opt_info like", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoNotLike(String value) {
            addCriterion("opt_info not like", value, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoIn(List<String> values) {
            addCriterion("opt_info in", values, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoNotIn(List<String> values) {
            addCriterion("opt_info not in", values, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoBetween(String value1, String value2) {
            addCriterion("opt_info between", value1, value2, "optInfo");
            return (Criteria) this;
        }

        public Criteria andOptInfoNotBetween(String value1, String value2) {
            addCriterion("opt_info not between", value1, value2, "optInfo");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Integer value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Integer value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Integer value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Integer value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Integer value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Integer> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Integer> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Integer value1, Integer value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Integer value1, Integer value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}