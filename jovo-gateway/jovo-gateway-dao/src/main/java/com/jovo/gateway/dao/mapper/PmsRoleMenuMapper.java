package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsRoleMenu;
import com.jovo.gateway.dao.model.PmsRoleMenuExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PmsRoleMenuMapper {
    long countByExample(PmsRoleMenuExample example);

    int delete(@Param("id") Integer id);

    int insert(PmsRoleMenu record);

    PmsRoleMenu selectPmsRoleMenuById(@Param("id") Integer id);

    List<PmsRoleMenu> getPmsRoleMenus();

    int update(PmsRoleMenu record);

    String selectPmsMenusByRoleId(Integer roleId);
}