package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsRolePms;
import com.jovo.gateway.dao.model.PmsRolePmsExample;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface PmsRolePmsMapper {
    long countByExample(PmsRolePmsExample example);

    int delete(@Param("id") Integer id);

    int insert(PmsRolePms record);

    int insertSelective(PmsRolePms record);

    List<PmsRolePms> selectByExample(PmsRolePmsExample example);

    int update(@Param("record") PmsRolePms record);

    int updateByExample(@Param("record") PmsRolePms record, @Param("example") PmsRolePmsExample example);

    String selectPmsPermissionsByRoleId(@Param("roleId") Integer roleId);

    PmsRolePms selectPmsMenusById(Integer id);

    int updateByRoleId(@Param("record") PmsRolePms pmsRolePms);
}