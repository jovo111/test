package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.dao.model.PmsUserExample;
import com.jovo.gateway.dao.query.page.PageQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface PmsUserMapper {
    long countByExample(PmsUserExample example);

    int deleteByExample(PmsUserExample example);

    int delete(@Param("uid") Integer uid);

    int insert(PmsUser record);

    List<PmsUser> selectByExample(PmsUserExample example);

    PmsUser selectPmsUserByUserId(@Param("uid") Integer uid);

    int update(@Param("record") PmsUser record);

    int updateByExample(@Param("record") PmsUser record, @Param("example") PmsUserExample example);

    PmsUser selectPmsUserByUserName(@Param("uname") String uname);

    List<PmsUser> getUsersByExample();
}