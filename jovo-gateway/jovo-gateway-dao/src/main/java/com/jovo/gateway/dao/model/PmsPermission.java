package com.jovo.gateway.dao.model;

import java.io.Serializable;
import java.util.List;

public class PmsPermission implements Serializable {
    /**
     * 目录ID
     *
     * @mbg.generated
     */
    private Integer menuId;

    /**
     * 目录名称
     *
     * @mbg.generated
     */
    private String menuName;

    /**
     * 组件名称
     *
     * @mbg.generated
     */
    private String componentName;

    /**
     * 组件
     *
     * @mbg.generated
     */
    private String component;

    /**
     * 路径
     *
     * @mbg.generated
     */
    private String path;

    /**
     * 图标
     *
     * @mbg.generated
     */
    private String icon;

    /**
     * 排序
     *
     * @mbg.generated
     */
    private Integer sort;

    /**
     * 上级目录ID
     *
     * @mbg.generated
     */
    private Integer pid;

    /**
     * 是否隐藏
     *
     * @mbg.generated
     */
    private Boolean hidden;

    /**
     * 是否开启vue 路由缓存
     *
     * @mbg.generated
     */
    private Boolean cache;

    /**
     * 权限类型，0:顶级目录，1:子目录，2:按钮或者其他显示控件
     *
     * @mbg.generated
     */
    private Integer type;

    /**
     * 是否是外链，0:否，1:是
     *
     * @mbg.generated
     */
    private Boolean iFrame;

    /**
     * 备注
     *
     * @mbg.generated
     */
    private String remark;

    /**
     * 权限code
     *
     * @mbg.generated
     */
    private String pmsCode;

    private String creator;

    private String editor;

    private Long createtime;

    private Long updatetime;

    private List<PmsPermission> children;

    private static final long serialVersionUID = -1739689563041078328L;

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getIFrame() {
        return iFrame;
    }

    public void setIFrame(Boolean iFrame) {
        this.iFrame = iFrame;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPmsCode() {
        return pmsCode;
    }

    public void setPmsCode(String pmsCode) {
        this.pmsCode = pmsCode;
    }

    public List<PmsPermission> getChildren() {
        return children;
    }

    public void setChildren(List<PmsPermission> children) {
        this.children = children;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public Long getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Long updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", menuId=").append(menuId);
        sb.append(", menuName=").append(menuName);
        sb.append(", componentName=").append(componentName);
        sb.append(", component=").append(component);
        sb.append(", path=").append(path);
        sb.append(", icon=").append(icon);
        sb.append(", sort=").append(sort);
        sb.append(", pid=").append(pid);
        sb.append(", hidden=").append(hidden);
        sb.append(", cache=").append(cache);
        sb.append(", createtime=").append(createtime);
        sb.append(", updatetime=").append(updatetime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PmsPermission other = (PmsPermission) that;
        return (this.getMenuId() == null ? other.getMenuId() == null : this.getMenuId().equals(other.getMenuId()))
            && (this.getMenuName() == null ? other.getMenuName() == null : this.getMenuName().equals(other.getMenuName()))
            && (this.getComponentName() == null ? other.getComponentName() == null : this.getComponentName().equals(other.getComponentName()))
            && (this.getComponent() == null ? other.getComponent() == null : this.getComponent().equals(other.getComponent()))
            && (this.getPath() == null ? other.getPath() == null : this.getPath().equals(other.getPath()))
            && (this.getIcon() == null ? other.getIcon() == null : this.getIcon().equals(other.getIcon()))
            && (this.getSort() == null ? other.getSort() == null : this.getSort().equals(other.getSort()))
            && (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
            && (this.getHidden() == null ? other.getHidden() == null : this.getHidden().equals(other.getHidden()))
            && (this.getCache() == null ? other.getCache() == null : this.getCache().equals(other.getCache()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getUpdatetime() == null ? other.getUpdatetime() == null : this.getUpdatetime().equals(other.getUpdatetime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getMenuId() == null) ? 0 : getMenuId().hashCode());
        result = prime * result + ((getMenuName() == null) ? 0 : getMenuName().hashCode());
        result = prime * result + ((getComponentName() == null) ? 0 : getComponentName().hashCode());
        result = prime * result + ((getComponent() == null) ? 0 : getComponent().hashCode());
        result = prime * result + ((getPath() == null) ? 0 : getPath().hashCode());
        result = prime * result + ((getIcon() == null) ? 0 : getIcon().hashCode());
        result = prime * result + ((getSort() == null) ? 0 : getSort().hashCode());
        result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
        result = prime * result + ((getHidden() == null) ? 0 : getHidden().hashCode());
        result = prime * result + ((getCache() == null) ? 0 : getCache().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getUpdatetime() == null) ? 0 : getUpdatetime().hashCode());
        return result;
    }
}