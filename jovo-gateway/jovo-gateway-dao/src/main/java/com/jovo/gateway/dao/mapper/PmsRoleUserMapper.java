package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsRoleUserExample;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface PmsRoleUserMapper {
    long countByExample(PmsRoleUserExample example);

    int delete(@Param("id") Integer id);

    int insert(PmsRoleUser record);

    int insertSelective(PmsRoleUser record);

    List<PmsRoleUser> selectByExample(PmsRoleUserExample example);

    int update(@Param("record") PmsRoleUser record);

    int updateByExample(@Param("record") PmsRoleUser record, @Param("example") PmsRoleUserExample example);

    Set<String> selectPmsRolesByUid(@Param("uid") Integer uid);

    int selectPmsRoleIdsByUid(@Param("uid") Integer uid);

    PmsRoleUser selectPmsRolesById(@Param("id") Integer id);

    int updateByUserId(PmsRoleUser pmsRoleUser);
}