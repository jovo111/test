package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.model.PmsRoleExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface PmsRoleMapper {
    long countByExample(PmsRoleExample example);

    int delete(@Param("roleId") Integer roleId);

    int insert(PmsRole record);

    int insertSelective(PmsRole record);

    List<PmsRole> selectByExample(PmsRoleExample example);

    int update(@Param("record") PmsRole record);

    int updateByExample(@Param("record") PmsRole record, @Param("example") PmsRoleExample example);

    PmsRole selectPmsRoleByRoleId(@Param("roleId") Integer roleId);

    List<PmsRole> getPmsRoles();
}