package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

public interface PmsPermissionMapper {
    long countByExample(PmsPermissionExample example);

    int delete(Integer menuId);

    int insert(PmsPermission record);

    List<PmsPermission> getPmsMenus();

    PmsPermission selectPmsMenuByMenuId(Integer menuId);

    int update(PmsPermission record);

    List<PmsPermission> getPmsMenusByPmsId(@Param("menuIds") String menuIds);

    Set<String> getPmsPermissionsByPmsId(@Param("pmsIds") String pmsIds);

    PmsPermission selectPmsPermissionByPmsId(Integer pmsId);

    List<PmsPermission> getAllPmsPermissions();
}