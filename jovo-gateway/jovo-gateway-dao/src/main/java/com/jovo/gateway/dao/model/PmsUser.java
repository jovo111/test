package com.jovo.gateway.dao.model;

import java.io.Serializable;
import java.util.List;

public class PmsUser implements Serializable {
    /**
     * 用户表
     *
     * @mbg.generated
     */
    private Integer userId;

    /**
     * 账号
     *
     * @mbg.generated
     */
    private String userName;

    /**
     * 用户名称
     *
     * @mbg.generated
     */
    private String nickname;

    /**
     * 密码
     *
     * @mbg.generated
     */
    private String password;

    /**
     * 用户头像
     *
     * @mbg.generated
     */
    private String avatar;

    /**
     * 用户状态，是否启用
     *
     * @mbg.generated
     */
    private Boolean status;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Long createtime;

    /**
     * 注册ip地址
     *
     * @mbg.generated
     */
    private String createip;

    /**
     * 最后登录时间
     *
     * @mbg.generated
     */
    private Long lastLoginTime;

    /**
     * 最后登录ip地址
     *
     * @mbg.generated
     */
    private String lastLoginIp;

    private PmsRoleUser role;

    private String roles;

    private static final long serialVersionUID = -4540033067655289872L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getCreateip() {
        return createip;
    }

    public void setCreateip(String createip) {
        this.createip = createip;
    }

    public Long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public PmsRoleUser getRole() {
        return role;
    }

    public void setRole(PmsRoleUser role) {
        this.role = role;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public PmsUser() {
    }

    public PmsUser(Integer userId, String userName, String nickname, String password, String avatar, boolean status, Long createtime, String createip, Long lastLoginTime, String lastLoginIp) {
        this.userId = userId;
        this.userName = userName;
        this.nickname = nickname;
        this.password = password;
        this.avatar = avatar;
        this.status = status;
        this.createtime = createtime;
        this.createip = createip;
        this.lastLoginTime = lastLoginTime;
        this.lastLoginIp = lastLoginIp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", nickname=").append(nickname);
        sb.append(", password=").append(password);
        sb.append(", avatar=").append(avatar);
        sb.append(", status=").append(status);
        sb.append(", createtime=").append(createtime);
        sb.append(", createip=").append(createip);
        sb.append(", lastLoginTime=").append(lastLoginTime);
        sb.append(", lastLoginIp=").append(lastLoginIp);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PmsUser other = (PmsUser) that;
        return (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getNickname() == null ? other.getNickname() == null : this.getNickname().equals(other.getNickname()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getAvatar() == null ? other.getAvatar() == null : this.getAvatar().equals(other.getAvatar()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getCreateip() == null ? other.getCreateip() == null : this.getCreateip().equals(other.getCreateip()))
            && (this.getLastLoginTime() == null ? other.getLastLoginTime() == null : this.getLastLoginTime().equals(other.getLastLoginTime()))
            && (this.getLastLoginIp() == null ? other.getLastLoginIp() == null : this.getLastLoginIp().equals(other.getLastLoginIp()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getNickname() == null) ? 0 : getNickname().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getAvatar() == null) ? 0 : getAvatar().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getCreateip() == null) ? 0 : getCreateip().hashCode());
        result = prime * result + ((getLastLoginTime() == null) ? 0 : getLastLoginTime().hashCode());
        result = prime * result + ((getLastLoginIp() == null) ? 0 : getLastLoginIp().hashCode());
        return result;
    }
}