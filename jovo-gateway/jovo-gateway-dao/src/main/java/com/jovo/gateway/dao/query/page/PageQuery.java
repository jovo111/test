package com.jovo.gateway.dao.query.page;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName PageQuery
 * @Description 分页实体类
 * @Author Jovo
 * @Date 2019-12-14 17:31
 * @Version V1.0
 */
@Data
public class PageQuery implements Serializable {

    private static final long serialVersionUID = -329295947575437182L;

    private int size = 30;

    private int page = 1;
}
