package com.jovo.gateway.dao.model;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class PmsOptLog implements Serializable {
    /**
     * 日志id
     *
     * @mbg.generated
     */
    private Integer logId;

    /**
     * 日志类型;INFO,ERROR
     *
     * @mbg.generated
     */
    private String logType;

    /**
     * 操作员名称
     *
     * @mbg.generated
     */
    private String userName;

    /**
     * 操作ip
     *
     * @mbg.generated
     */
    private String optIp;

    /**
     * 请求地址
     *
     * @mbg.generated
     */
    private String optAddress;

    /**
     * 浏览器名称
     *
     * @mbg.generated
     */
    private String optBrowser;

    /**
     * 系统名称
     *
     * @mbg.generated
     */
    private String optOs;

    /**
     * 调用方法名
     *
     * @mbg.generated
     */
    private String optMethod;

    /**
     * 调用参数
     *
     * @mbg.generated
     */
    private String optParams;

    /**
     * 异常信息
     *
     * @mbg.generated
     */
    private String optException;

    /**
     * 操作信息
     *
     * @mbg.generated
     */
    private String optInfo;

    /**
     * 请求耗时
     *
     * @mbg.generated
     */
    private Long optTime;

    private Long createtime;

    private static final long serialVersionUID = 4787313372037554177L;

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOptIp() {
        return optIp;
    }

    public void setOptIp(String optIp) {
        this.optIp = optIp;
    }

    public String getOptAddress() {
        return optAddress;
    }

    public void setOptAddress(String optAddress) {
        this.optAddress = optAddress;
    }

    public String getOptBrowser() {
        return optBrowser;
    }

    public void setOptBrowser(String optBrowser) {
        this.optBrowser = optBrowser;
    }

    public String getOptOs() {
        return optOs;
    }

    public void setOptOs(String optOs) {
        this.optOs = optOs;
    }

    public String getOptMethod() {
        return optMethod;
    }

    public void setOptMethod(String optMethod) {
        this.optMethod = optMethod;
    }

    public String getOptParams() {
        return optParams;
    }

    public void setOptParams(String optParams) {
        this.optParams = optParams;
    }

    public String getOptException() {
        return optException;
    }

    public void setOptException(String optException) {
        this.optException = optException;
    }

    public String getOptInfo() {
        return optInfo;
    }

    public void setOptInfo(String optInfo) {
        this.optInfo = optInfo;
    }

    public Long getOptTime() {
        return optTime;
    }

    public void setOptTime(Long optTime) {
        this.optTime = optTime;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public PmsOptLog(String logType, String userName, String optIp, String optAddress, String optBrowser, String optOs, Long optTime, Long createtime) {
        this.logType = logType;
        this.userName = userName;
        this.optIp = optIp;
        this.optAddress = optAddress;
        this.optBrowser = optBrowser;
        this.optOs = optOs;
        this.optTime = optTime;
        this.createtime = createtime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", logId=").append(logId);
        sb.append(", logType=").append(logType);
        sb.append(", userName=").append(userName);
        sb.append(", optIp=").append(optIp);
        sb.append(", optAddress=").append(optAddress);
        sb.append(", optBrowser=").append(optBrowser);
        sb.append(", optOs=").append(optOs);
        sb.append(", optMethod=").append(optMethod);
        sb.append(", optParams=").append(optParams);
        sb.append(", optException=").append(optException);
        sb.append(", optInfo=").append(optInfo);
        sb.append(", optTime=").append(optTime);
        sb.append(", createtime=").append(createtime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PmsOptLog other = (PmsOptLog) that;
        return (this.getLogId() == null ? other.getLogId() == null : this.getLogId().equals(other.getLogId()))
            && (this.getLogType() == null ? other.getLogType() == null : this.getLogType().equals(other.getLogType()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getOptIp() == null ? other.getOptIp() == null : this.getOptIp().equals(other.getOptIp()))
            && (this.getOptAddress() == null ? other.getOptAddress() == null : this.getOptAddress().equals(other.getOptAddress()))
            && (this.getOptBrowser() == null ? other.getOptBrowser() == null : this.getOptBrowser().equals(other.getOptBrowser()))
            && (this.getOptOs() == null ? other.getOptOs() == null : this.getOptOs().equals(other.getOptOs()))
            && (this.getOptMethod() == null ? other.getOptMethod() == null : this.getOptMethod().equals(other.getOptMethod()))
            && (this.getOptParams() == null ? other.getOptParams() == null : this.getOptParams().equals(other.getOptParams()))
            && (this.getOptException() == null ? other.getOptException() == null : this.getOptException().equals(other.getOptException()))
            && (this.getOptInfo() == null ? other.getOptInfo() == null : this.getOptInfo().equals(other.getOptInfo()))
            && (this.getOptTime() == null ? other.getOptTime() == null : this.getOptTime().equals(other.getOptTime()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getLogId() == null) ? 0 : getLogId().hashCode());
        result = prime * result + ((getLogType() == null) ? 0 : getLogType().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getOptIp() == null) ? 0 : getOptIp().hashCode());
        result = prime * result + ((getOptAddress() == null) ? 0 : getOptAddress().hashCode());
        result = prime * result + ((getOptBrowser() == null) ? 0 : getOptBrowser().hashCode());
        result = prime * result + ((getOptOs() == null) ? 0 : getOptOs().hashCode());
        result = prime * result + ((getOptMethod() == null) ? 0 : getOptMethod().hashCode());
        result = prime * result + ((getOptParams() == null) ? 0 : getOptParams().hashCode());
        result = prime * result + ((getOptException() == null) ? 0 : getOptException().hashCode());
        result = prime * result + ((getOptInfo() == null) ? 0 : getOptInfo().hashCode());
        result = prime * result + ((getOptTime() == null) ? 0 : getOptTime().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        return result;
    }
}