package com.jovo.gateway.dao.mapper;

import com.jovo.gateway.dao.model.PmsOptLog;
import com.jovo.gateway.dao.model.PmsOptLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PmsOptLogMapper {

    long countByExample(PmsOptLogExample example);

    int insert(PmsOptLog record);

    List<PmsOptLog> selectByExample(PmsOptLogExample example);

    List<PmsOptLog> getPmsOptLogs();
}