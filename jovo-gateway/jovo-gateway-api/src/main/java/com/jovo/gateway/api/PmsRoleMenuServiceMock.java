package com.jovo.gateway.api;

import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsRoleMenuMapper;
import com.jovo.gateway.dao.model.PmsRoleMenu;
import com.jovo.gateway.dao.model.PmsRoleMenuExample;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
* 降级实现PmsRoleMenuService接口
* Created by jovo on 2019/11/29.
*/
@Slf4j
public class PmsRoleMenuServiceMock extends BaseServiceMock<PmsRoleMenuMapper, PmsRoleMenu, PmsRoleMenuExample> implements PmsRoleMenuService {

    @Override
    public List<PmsRoleMenu> getPmsRoleMenus() {
        return null;
    }

    @Override
    public int delete(Integer menuId) {
        return 0;
    }

    @Override
    public PmsRoleMenu selectPmsRoleMenuById(Integer menuId) {
        return null;
    }

    @Override
    public int update(PmsRoleMenu pmsRoleMenu) {
        return 0;
    }
}
