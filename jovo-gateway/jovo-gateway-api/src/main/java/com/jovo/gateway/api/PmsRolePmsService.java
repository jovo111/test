package com.jovo.gateway.api;

import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsRolePms;
import com.jovo.gateway.dao.model.PmsRolePmsExample;
import java.util.Set;

/**
* PmsRolePmsService接口
* Created by jovo on 2019/9/25.
*/
public interface PmsRolePmsService extends BaseService<PmsRolePms, PmsRolePmsExample> {

    /**
     * @Author Jovo
     * @Description 获取所有权限CODE
     * @Date 16:16 2019-10-23
     * @Param [uid]
     * @return java.util.Set<java.lang.String>
     **/
    Set<String> selectPmsMenusByRoleId(Integer uid);

    PmsRolePms selectPmsMenusById(Integer id);

    int update(PmsRolePms pmsRolePms);

    int delete(Integer id);

    int updateByRoleId(PmsRolePms pmsRolePms);
}