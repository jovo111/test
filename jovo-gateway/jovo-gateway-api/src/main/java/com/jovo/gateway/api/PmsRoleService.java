package com.jovo.gateway.api;

import com.github.pagehelper.PageInfo;
import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.model.PmsRoleExample;
import com.jovo.gateway.dao.query.page.PageQuery;

import java.util.List;

/**
* PmsRoleService接口
* Created by jovo on 2019/9/25.
*/
public interface PmsRoleService extends BaseService<PmsRole, PmsRoleExample> {

    /**
     * 根据角色id获取角色信息
     * @param roleId
     * @return
     */
    PmsRole selectPmsRoleByRoleId(Integer roleId);

    /**
     * 查询所有角色列表
     * @return
     * @param pageQuery
     */
    PageInfo<PmsRole> getPmsRoles(PageQuery pageQuery);

    /**
     * 获取所有角色用于选择器使用
     * @return
     * @param
     */
    List<PmsRole> getPmsRolesBySelect();

    int update(PmsRole pmsRole);

    int delete(Integer roleId);
}