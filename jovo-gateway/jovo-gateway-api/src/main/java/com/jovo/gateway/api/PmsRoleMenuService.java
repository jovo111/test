package com.jovo.gateway.api;

import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsRoleMenu;
import com.jovo.gateway.dao.model.PmsRoleMenuExample;

import java.util.List;

/**
* PmsRoleMenuService接口
* Created by jovo on 2019/11/29.
*/
public interface PmsRoleMenuService extends BaseService<PmsRoleMenu, PmsRoleMenuExample> {

    List<PmsRoleMenu> getPmsRoleMenus();

    int delete(Integer menuId);

    PmsRoleMenu selectPmsRoleMenuById(Integer Id);

    int update(PmsRoleMenu pmsRoleMenu);

    int insert(PmsRoleMenu pmsRoleMenu);
}