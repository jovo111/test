package com.jovo.gateway.api;

import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsRoleUserMapper;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsRoleUserExample;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Set;

/**
* 降级实现PmsRoleUserService接口
* Created by jovo on 2019/9/25.
*/
@Slf4j
public class PmsRoleUserServiceMock extends BaseServiceMock<PmsRoleUserMapper, PmsRoleUser, PmsRoleUserExample> implements PmsRoleUserService {

    @Override
    public Set<String> selectPmsPermissionsByUid(Integer uid) {
        log.info("----------- PmsRoleUserServiceMock => selectPmsPermissionsByUid");
        return null;
    }

    @Override
    public Set<String> selectPmsRolesByUid(Integer uid) {
        log.info("----------- PmsRoleUserServiceMock => selectPmsRolesByUid");
        return null;
    }

    @Override
    public PmsRoleUser selectPmsRoleById(Integer id) {
        log.info("----------- PmsRoleUserServiceMock => selectPmsRoleById");
        return null;
    }

    @Override
    public int update(PmsRoleUser pmsRoleUser) {
        log.info("----------- PmsRoleUserServiceMock => update");
        return 0;
    }

    @Override
    public int delete(Integer id) {
        log.info("----------- PmsRoleUserServiceMock => delete");
        return 0;
    }

    @Override
    public List<PmsPermission> getPmsMenusByUserId(int uid) {
        log.info("----------- PmsRoleUserServiceMock => getPmsMenusByUserId");
        return null;
    }

    @Override
    public int updateByUserId(PmsRoleUser pmsRoleUser) {
        log.info("----------- PmsRoleUserServiceMock => updateByUserId");
        return 0;
    }
}
