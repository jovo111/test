package com.jovo.gateway.api;

import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsPermissionMapper;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
* 降级实现PmsMenuService接口
* Created by jovo on 2019/11/29.
*/
@Slf4j
public class PmsMenuServiceMock extends BaseServiceMock<PmsPermissionMapper, PmsPermission, PmsPermissionExample> implements PmsMenuService {

    @Override
    public List<PmsPermission> getPmsMenus() {
        return null;
    }

    @Override
    public PmsPermission selectPmsMenuByMenuId(Integer menuId) {
        return null;
    }

    @Override
    public int delete(Integer menuId) {
        return 0;
    }

    @Override
    public int update(PmsPermission pmsPermission) {
        return 0;
    }
}
