package com.jovo.gateway.api;

import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsRolePmsMapper;
import com.jovo.gateway.dao.model.PmsRolePms;
import com.jovo.gateway.dao.model.PmsRolePmsExample;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

/**
* 降级实现PmsRolePmsService接口
* Created by jovo on 2019/9/25.
*/
@Slf4j
public class PmsRolePmsServiceMock extends BaseServiceMock<PmsRolePmsMapper, PmsRolePms, PmsRolePmsExample> implements PmsRolePmsService {

    @Override
    public Set<String> selectPmsMenusByRoleId(Integer uid) {
        log.info("----------- PmsRolePmsServiceMock => selectPmsMenusByRoleId");
        return null;
    }

    @Override
    public PmsRolePms selectPmsMenusById(Integer id) {
        log.info("----------- PmsRolePmsServiceMock => selectPmsMenusById");
        return null;
    }

    @Override
    public int update(PmsRolePms pmsRolePms) {
        log.info("----------- PmsRolePmsServiceMock => update");
        return 0;
    }

    @Override
    public int delete(Integer id) {
        log.info("----------- PmsRolePmsServiceMock => delete");
        return 0;
    }

    @Override
    public int updateByRoleId(PmsRolePms pmsRolePms) {
        log.info("----------- PmsRolePmsServiceMock => updateByRoleId");
        return 0;
    }

    @Override
    public int insert(PmsRolePms pmsRolePms) {
        log.info("----------- PmsRolePmsServiceMock => insert");
        return 0;
    }
}
