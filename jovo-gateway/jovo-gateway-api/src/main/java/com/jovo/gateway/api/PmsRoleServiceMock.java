package com.jovo.gateway.api;

import com.github.pagehelper.PageInfo;
import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsRoleMapper;
import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.model.PmsRoleExample;
import com.jovo.gateway.dao.query.page.PageQuery;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
* 降级实现PmsRoleService接口
* Created by jovo on 2019/9/25.
*/
@Slf4j
public class PmsRoleServiceMock extends BaseServiceMock<PmsRoleMapper, PmsRole, PmsRoleExample> implements PmsRoleService {

    @Override
    public PmsRole selectPmsRoleByRoleId(Integer roleId) {
        log.info("----------- PmsRoleServiceMock => selectPmsRoleByRoleId");
        return null;
    }

    @Override
    public PageInfo<PmsRole> getPmsRoles(PageQuery pageQuery) {
        log.info("----------- PmsRoleServiceMock => getPmsRoles");
        return null;
    }

    @Override
    public List<PmsRole> getPmsRolesBySelect() {
        log.info("----------- PmsRoleServiceMock => getPmsRolesBySelect");
        return null;
    }

    @Override
    public int update(PmsRole pmsRole) {
        log.info("----------- PmsRoleServiceMock => update");
        return 0;
    }

    @Override
    public int delete(Integer roleId) {
        log.info("----------- PmsRoleServiceMock => delete");
        return 0;
    }
}
