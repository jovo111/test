package com.jovo.gateway.api;

import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsPermissionMapper;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
* 降级实现PmsPermissionService接口
* Created by jovo on 2019/9/25.
*/
@Slf4j
public class PmsPermissionServiceMock extends BaseServiceMock<PmsPermissionMapper, PmsPermission, PmsPermissionExample> implements PmsPermissionService {

    @Override
    public PmsPermission selectPmsPermissionByPmsId(Integer pmsId) {
        log.info("----------- PmsPermissionServiceMock => selectPmsPermissionByPmsId");
        return null;
    }

    @Override
    public List<PmsPermission> getPmsPermissions() {
        log.info("----------- PmsPermissionServiceMock => getPmsPermissions");
        return null;
    }

    @Override
    public int update(PmsPermission pmsPermission) {
        log.info("----------- PmsPermissionServiceMock => update");
        return 0;
    }

    @Override
    public int delete(int pmsId) {
        log.info("----------- PmsPermissionServiceMock => delete");
        return 0;
    }
}
