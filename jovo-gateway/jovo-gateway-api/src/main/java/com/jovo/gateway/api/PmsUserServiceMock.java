package com.jovo.gateway.api;

import com.github.pagehelper.PageInfo;
import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsUserMapper;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.dao.model.PmsUserExample;
import com.jovo.gateway.dao.query.page.PageQuery;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
* 降级实现PmsUserService接口
* Created by jovo on 2019/9/25.
*/
@Slf4j
public class PmsUserServiceMock extends BaseServiceMock<PmsUserMapper, PmsUser, PmsUserExample> implements PmsUserService {

    @Override
    public PmsUser selectPmsUserByUserId(Integer uid) {
        log.info("----------- PmsUserServiceMock => selectPmsUserByUserId");
        return null;
    }

    @Override
    public PmsUser selectPmsUserByUserName(String uname) {
        log.info("----------- PmsUserServiceMock => selectPmsUserByUserName");
        return null;
    }

    @Override
    public int update(PmsUser pmsUser) {
        log.info("----------- PmsUserServiceMock => update");
        return 0;
    }

    @Override
    public int delete(Integer uid) {
        log.info("----------- PmsUserServiceMock => delete");
        return 0;
    }

    @Override
    public PageInfo<PmsUser> getUsersByExample(PageQuery pageQuery) {
        log.info("----------- PmsUserServiceMock => getUsersByExample");
        return null;
    }

    @Override
    public int insertUserRole(PmsUser pmsUser, PmsRoleUser pmsRoleUser) {
        log.info("----------- PmsUserServiceMock => insertUserRole");
        return 0;
    }
}
