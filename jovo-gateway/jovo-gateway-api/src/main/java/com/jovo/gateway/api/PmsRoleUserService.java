package com.jovo.gateway.api;

import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsRoleUserExample;

import java.util.List;
import java.util.Set;

/**
* PmsRoleUserService接口
* Created by jovo on 2019/9/25.
*/
public interface PmsRoleUserService extends BaseService<PmsRoleUser, PmsRoleUserExample> {

    /**
     * 根据用户id查询所有权限
     * @param uid
     * @return
     */
    Set<String> selectPmsPermissionsByUid(Integer uid);

    /**
     * 根据用户id查询所有角色
     * @param uid
     * @return
     */
    Set<String> selectPmsRolesByUid(Integer uid);

    PmsRoleUser selectPmsRoleById(Integer id);

    int update(PmsRoleUser pmsRoleUser);

    int delete(Integer id);

    /**
     * @Author Jovo
     * @Description 根据用户id查询所有目录
     * @Date 16:46 2019-11-29
     * @Param [uid]
     * @return java.util.List<com.jovo.gateway.dao.model.PmsMenu>
     **/
    List<PmsPermission> getPmsMenusByUserId(int uid);

    int updateByUserId(PmsRoleUser pmsRoleUser);
}