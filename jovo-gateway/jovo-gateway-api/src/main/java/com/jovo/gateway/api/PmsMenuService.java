package com.jovo.gateway.api;

import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;

import java.util.List;

/**
* PmsMenuService接口
* Created by jovo on 2019/11/29.
*/
public interface PmsMenuService extends BaseService<PmsPermission, PmsPermissionExample> {

    List<PmsPermission> getPmsMenus();

    PmsPermission selectPmsMenuByMenuId(Integer menuId);

    int delete(Integer menuId);

    int update(PmsPermission pmsPermission);
}