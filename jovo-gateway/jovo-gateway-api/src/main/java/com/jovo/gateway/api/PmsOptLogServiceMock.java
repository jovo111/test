package com.jovo.gateway.api;

import com.github.pagehelper.PageInfo;
import com.jovo.common.base.BaseServiceMock;
import com.jovo.gateway.dao.mapper.PmsOptLogMapper;
import com.jovo.gateway.dao.model.PmsOptLog;
import com.jovo.gateway.dao.model.PmsOptLogExample;
import com.jovo.gateway.dao.query.page.PageQuery;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
* 降级实现PmsOptLogService接口
* Created by jovo on 2019/12/16.
*/
@Slf4j
public class PmsOptLogServiceMock extends BaseServiceMock<PmsOptLogMapper, PmsOptLog, PmsOptLogExample> implements PmsOptLogService {

    @Override
    public PageInfo<PmsOptLog> getPmsOptLogs(PageQuery pageQuery) {
        return null;
    }
}
