package com.jovo.gateway.api;

import com.github.pagehelper.PageInfo;
import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.dao.model.PmsUserExample;
import com.jovo.gateway.dao.query.page.PageQuery;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* PmsUserService接口
* Created by jovo on 2019/9/25.
*/
public interface PmsUserService extends BaseService<PmsUser, PmsUserExample> {

    /**
     * 根据用户id获取用户信息
     * @param uid
     * @return
     */
    PmsUser selectPmsUserByUserId(Integer uid);

    /**
     * 根据用户名获取用户信息
     * @param uname
     * @return
     */
    PmsUser selectPmsUserByUserName(String uname);

    /**
     * 修改用户数据
     * @param pmsUser
     * @return
     */
    int update(PmsUser pmsUser);

    /**
     * 删除用户信息
     * @param uid
     * @return
     */
    int delete(Integer uid);

    /**
     * @Author Jovo
     * @Description 根据条件获取用户
     * @Date 17:41 2019-12-02
     * @Param []
     * @return java.util.List<com.jovo.gateway.dao.model.PmsUser>
     **/
    PageInfo<PmsUser> getUsersByExample(PageQuery pageQuery);

    int insertUserRole(PmsUser pmsUser, PmsRoleUser pmsRoleUser);
}