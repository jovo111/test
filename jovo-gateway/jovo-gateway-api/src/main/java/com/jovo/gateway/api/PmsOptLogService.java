package com.jovo.gateway.api;

import com.github.pagehelper.PageInfo;
import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsOptLog;
import com.jovo.gateway.dao.model.PmsOptLogExample;
import com.jovo.gateway.dao.query.page.PageQuery;

import java.util.List;

/**
* PmsOptLogService接口
* Created by jovo on 2019/12/16.
*/
public interface PmsOptLogService extends BaseService<PmsOptLog, PmsOptLogExample> {

    PageInfo<PmsOptLog> getPmsOptLogs(PageQuery pageQuery);
}