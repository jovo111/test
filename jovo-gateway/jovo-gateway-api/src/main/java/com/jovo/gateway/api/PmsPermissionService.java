package com.jovo.gateway.api;

import com.jovo.common.base.BaseService;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;

import java.util.List;

/**
* PmsPermissionService接口
* Created by jovo on 2019/9/25.
*/
public interface PmsPermissionService extends BaseService<PmsPermission, PmsPermissionExample> {

    /**
     * 根据权限id查询权限信息
     * @param pmsId
     * @return
     */
    PmsPermission selectPmsPermissionByPmsId(Integer pmsId);

    /**
     * 查询所有权限列表
     * @return
     */
    List<PmsPermission> getPmsPermissions();

    int update(PmsPermission pmsPermission);

    int delete(int pmsId);
}