package com.jovo.gateway.service;

import com.alibaba.fastjson.JSON;
import com.jovo.gateway.api.PmsPermissionService;
import com.jovo.gateway.api.PmsRolePmsService;
import com.jovo.gateway.api.PmsRoleService;
import com.jovo.gateway.api.PmsRoleUserService;
import com.jovo.gateway.dao.model.PmsRoleUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

/**
 * @ClassName PmsRoleTest
 * @Description TODO
 * @Author Jovo
 * @Date 2019-10-22 15:08
 * @Version V1.0
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PmsTests {

    @Autowired
    private PmsRoleService pmsRoleService;

    @Autowired
    private PmsPermissionService pmsPermissionService;

    @Autowired
    private PmsRolePmsService pmsRolePmsService;

    @Autowired
    private PmsRoleUserService pmsRoleUserService;

    @Test
    public void insert(){
        Long timestamp = System.currentTimeMillis()/1000;
//        PmsRole pmsRole = new PmsRole("admin", 1, "超管", timestamp, "jovo", timestamp, "jovo", 1);
//        PmsRole pmsRole1 = new PmsRole("user1", 1, "管理员1", timestamp, "jovo", timestamp, "jovo", 0);
//        PmsRole pmsRole2 = new PmsRole("user2", 1, "管理员2", timestamp, "jovo", timestamp, "jovo", 0);
//
//        pmsRoleService.insert(pmsRole);
//        pmsRoleService.insert(pmsRole1);
//        pmsRoleService.insert(pmsRole2);

//        PmsPermission pmsPermission = new PmsPermission(1L,"jovo",timestamp,"jovo",timestamp,1,"添加管理员权限","添加管理员","pms:user:create");
//        PmsPermission pmsPermission1 = new PmsPermission(1L,"jovo",timestamp,"jovo",timestamp,1,"修改管理员权限","修改管理员","pms:user:update");
//        PmsPermission pmsPermission2 = new PmsPermission(1L,"jovo",timestamp,"jovo",timestamp,1,"删除管理员权限","删除管理员","pms:user:delete");
//        PmsPermission pmsPermission3 = new PmsPermission(1L,"jovo",timestamp,"jovo",timestamp,1,"查看管理员权限","查看管理员","pms:user:read");
//        PmsPermission pmsPermission4 = new PmsPermission(1L,"jovo",timestamp,"jovo",timestamp,1,"test","teset","test");
//
//        pmsPermissionService.insert(pmsPermission);
//        pmsPermissionService.insert(pmsPermission1);
//        pmsPermissionService.insert(pmsPermission2);
//        pmsPermissionService.insert(pmsPermission3);
//        pmsPermissionService.insert(pmsPermission4);

//        PmsRolePms pmsRolePms = new PmsRolePms(1,"1,2,3,4",timestamp,timestamp,"jovo","jovo","超管权限");
//        PmsRolePms pmsRolePms1 = new PmsRolePms(2,"4",timestamp,timestamp,"jovo","jovo","管理员1权限");
//        PmsRolePms pmsRolePms2 = new PmsRolePms(3,"4",timestamp,timestamp,"jovo","jovo","管理员2权限");
//
//        pmsRolePmsService.insert(pmsRolePms);
//        pmsRolePmsService.insert(pmsRolePms1);
//        pmsRolePmsService.insert(pmsRolePms2);

        PmsRoleUser pmsRoleUser = new PmsRoleUser(3,"1","超管",timestamp,timestamp,"jovo","jovo");
        PmsRoleUser pmsRoleUser1 = new PmsRoleUser(2,"2","管理",timestamp,timestamp,"jovo","jovo");
        PmsRoleUser pmsRoleUser2 = new PmsRoleUser(4,"2","管理",timestamp,timestamp,"jovo","jovo");

        pmsRoleUserService.insert(pmsRoleUser);
        pmsRoleUserService.insert(pmsRoleUser1);
        pmsRoleUserService.insert(pmsRoleUser2);
    }

    @Test
    public void getList(){
//        List<PmsRole> roles = pmsRoleService.getPmsRoles();
//        log.info(JSON.toJSONString(roles));

//        List<PmsPermission> pmsList = pmsPermissionService.getPmsPermissions();
//        log.info(JSON.toJSONString(pmsList));

//        Set<String> pmsList = pmsRolePmsService.selectPmsMenusByRoleId(1);
//        log.info(JSON.toJSONString(pmsList));

        Set<String> pmsList = pmsRoleUserService.selectPmsPermissionsByUid(3);
        Set<String> roleList = pmsRoleUserService.selectPmsRolesByUid(3);
        log.info(JSON.toJSONString(pmsList));
        log.info(JSON.toJSONString(roleList));
    }

    @Test
    public void update(){
//        PmsRole role = pmsRoleService.selectPmsRoleByRoleId(2);
//        role.setRemark("测试");
//        pmsRoleService.update(role);

        Long timestamp = System.currentTimeMillis()/1000;
//        PmsPermission pmsPermission = pmsPermissionService.selectPmsPermissionByPmsId(4);
//        pmsPermission.setVersion(2L);
//        pmsPermission.setEditor("jovo");
//        pmsPermission.setUpdatetime(timestamp);
//
//        log.info("修改回调:{}",pmsPermissionService.update(pmsPermission));

//        PmsRolePms pmsRolePms = pmsRolePmsService.selectPmsMenusById(2);
//        pmsRolePms.setMenus("1,2,3,4");
//        pmsRolePms.setEditor("jovo");
//        pmsRolePms.setUpdatetime(timestamp);

//        log.info("修改回调:{}",pmsRolePmsService.update(pmsRolePms));

        PmsRoleUser pmsRoleUser = pmsRoleUserService.selectPmsRoleById(3);
        pmsRoleUser.setEditor("jovo");
        pmsRoleUser.setRoles("1");
        pmsRoleUser.setUpdatetime(timestamp);

        pmsRoleUserService.update(pmsRoleUser);
    }

    @Test
    public void delete(){
//        pmsRoleService.delete(3);

//        log.info("修改回调:{}",pmsPermissionService.delete(5));

        log.info("修改回调:{}",pmsRoleUserService.delete(4));


    }
}
