package com.jovo.gateway.service;

import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.dao.model.PmsUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class GatewayServiceApplicationTests {

    @Autowired
    private PmsUserService pmsUserService;

    @Test
    public void insert() {
        Long timeStamp = System.currentTimeMillis()/1000;
        PmsUser pmsUser = new PmsUser(1,"","lisi","123456","hello",true,timeStamp,"127.0.0.1",timeStamp,"127.0.0.1");
        PmsUser pmsUser1 = new PmsUser(1,"","wanglizhi","123456","hello",true,timeStamp,"127.0.0.1",timeStamp,"127.0.0.1");
        pmsUserService.insert(pmsUser);
        pmsUserService.insert(pmsUser1);
    }

    @Test
    public void get() {
        PmsUser pmsUser = pmsUserService.selectPmsUserByUserId(2);
        assertEquals("lisi",pmsUser.getNickname());
    }

    @Test
    public void update() {
        Long timeStamp = System.currentTimeMillis()/1000;
        PmsUser pmsUser = new PmsUser(1,"","zhangsan111","123456","hello",true,timeStamp,"127.0.0.1",timeStamp,"127.0.0.1");
        pmsUserService.update(pmsUser);
    }

    @Test
    public void delete() {
        pmsUserService.delete(1);
    }

}
