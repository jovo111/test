package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsRoleMenuMapper;
import com.jovo.gateway.dao.model.PmsRoleMenu;
import com.jovo.gateway.dao.model.PmsRoleMenuExample;
import com.jovo.gateway.api.PmsRoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* PmsRoleMenuService实现
* Created by jovo on 2019/11/29.
*/
@Slf4j
@Service
@Component
@Transactional(rollbackFor = Exception.class)
@BaseService
public class PmsRoleMenuServiceImpl extends BaseServiceImpl<PmsRoleMenuMapper, PmsRoleMenu, PmsRoleMenuExample> implements PmsRoleMenuService {

    @Autowired
    PmsRoleMenuMapper pmsRoleMenuMapper;

    @Override
    public List<PmsRoleMenu> getPmsRoleMenus() {
        return pmsRoleMenuMapper.getPmsRoleMenus();
    }

    @Override
    public int delete(Integer id) {
        return pmsRoleMenuMapper.delete(id);
    }

    @Override
    public PmsRoleMenu selectPmsRoleMenuById(Integer id) {
        return pmsRoleMenuMapper.selectPmsRoleMenuById(id);
    }

    @Override
    public int update(PmsRoleMenu pmsRoleMenu) {
        return pmsRoleMenuMapper.update(pmsRoleMenu);
    }

    @Override
    public int insert(PmsRoleMenu pmsRoleMenu) {
        return pmsRoleMenuMapper.insert(pmsRoleMenu);
    }
}