package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsPermissionMapper;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;
import com.jovo.gateway.api.PmsPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * PmsPermissionService实现
 * Created by jovo on 2019/9/25.
 */
@Slf4j
@Service
@Component
@Transactional(rollbackFor = Exception.class)
@BaseService
@CacheConfig(cacheNames = "jovo-gateway-web-cache-pmss")
public class PmsPermissionServiceImpl extends BaseServiceImpl<PmsPermissionMapper, PmsPermission, PmsPermissionExample> implements PmsPermissionService {

    @Autowired
    PmsPermissionMapper pmsPermissionMapper;

    @Override
    public PmsPermission selectPmsPermissionByPmsId(Integer pmsId) {
        return pmsPermissionMapper.selectPmsPermissionByPmsId(pmsId);
    }

    @Override
    @Cacheable
    public List<PmsPermission> getPmsPermissions() {
        return pmsPermissionMapper.getAllPmsPermissions();
    }

    @Override
    @CacheEvict(allEntries = true)
    public int update(PmsPermission pmsPermission) {
        return pmsPermissionMapper.update(pmsPermission);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int delete(int pmsId) {
        return pmsPermissionMapper.delete(pmsId);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int insert(PmsPermission pmsPermission) {
        return pmsPermissionMapper.insert(pmsPermission);
    }
}