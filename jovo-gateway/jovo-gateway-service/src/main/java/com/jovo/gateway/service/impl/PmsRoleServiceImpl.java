package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsRoleMapper;
import com.jovo.gateway.dao.model.PmsRole;
import com.jovo.gateway.dao.model.PmsRoleExample;
import com.jovo.gateway.api.PmsRoleService;
import com.jovo.gateway.dao.query.page.PageQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* PmsRoleService实现
*
 * @author jovo
 * @date 2019/9/25
 */
@Slf4j
@Service
@Component
@Transactional
@BaseService
@CacheConfig(cacheNames = "jovo-gateway-web-cache-role")
public class PmsRoleServiceImpl extends BaseServiceImpl<PmsRoleMapper, PmsRole, PmsRoleExample> implements PmsRoleService {

    @Autowired
    PmsRoleMapper pmsRoleMapper;

    @Override
    public PmsRole selectPmsRoleByRoleId(Integer roleId) {
        return pmsRoleMapper.selectPmsRoleByRoleId(roleId);
    }

    @Override
    @Cacheable
    public PageInfo<PmsRole> getPmsRoles(PageQuery pageQuery) {
        PageHelper.startPage(pageQuery.getPage(),pageQuery.getSize());
        List<PmsRole> pmsRoles = pmsRoleMapper.getPmsRoles();
        return new PageInfo<>(pmsRoles);
    }

    @Override
    public List<PmsRole> getPmsRolesBySelect() {
        return pmsRoleMapper.getPmsRoles();
    }

    @Override
    @CacheEvict(allEntries = true)
    public int insert(PmsRole pmsRole) {
        return pmsRoleMapper.insert(pmsRole);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int update(PmsRole pmsRole) {
        return pmsRoleMapper.update(pmsRole);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int delete(Integer roleId) {
        return pmsRoleMapper.delete(roleId);
    }
}