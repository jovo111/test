package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsPermissionMapper;
import com.jovo.gateway.dao.mapper.PmsRolePmsMapper;
import com.jovo.gateway.dao.model.PmsRolePms;
import com.jovo.gateway.dao.model.PmsRolePmsExample;
import com.jovo.gateway.api.PmsRolePmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
* PmsRolePmsService实现
* Created by jovo on 2019/9/25.
*/
@Slf4j
@Service
@Component
@Transactional
@BaseService
public class PmsRolePmsServiceImpl extends BaseServiceImpl<PmsRolePmsMapper, PmsRolePms, PmsRolePmsExample> implements PmsRolePmsService {

    @Autowired
    PmsRolePmsMapper pmsRolePmsMapper;

    @Autowired
    PmsPermissionMapper pmsPermissionMapper;

    @Override
    public Set<String> selectPmsMenusByRoleId(Integer roleId) {
        String permissions = pmsRolePmsMapper.selectPmsPermissionsByRoleId(roleId);
        Set<String> pmsLst = pmsPermissionMapper.getPmsPermissionsByPmsId(permissions);
        return pmsLst;
    }

    @Override
    public PmsRolePms selectPmsMenusById(Integer id) {
        return pmsRolePmsMapper.selectPmsMenusById(id);
    }

    @Override
    public int insert(PmsRolePms pmsRolePms) {
        return pmsRolePmsMapper.insert(pmsRolePms);
    }

    @Override
    public int update(PmsRolePms pmsRolePms) {
        return pmsRolePmsMapper.update(pmsRolePms);
    }

    @Override
    public int delete(Integer id) {
        return pmsRolePmsMapper.delete(id);
    }

    @Override
    public int updateByRoleId(PmsRolePms pmsRolePms) {
        return pmsRolePmsMapper.updateByRoleId(pmsRolePms);
    }
}