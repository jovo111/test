package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsOptLogMapper;
import com.jovo.gateway.dao.model.PmsOptLog;
import com.jovo.gateway.dao.model.PmsOptLogExample;
import com.jovo.gateway.api.PmsOptLogService;
import com.jovo.gateway.dao.query.page.PageQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* PmsOptLogService实现
* Created by jovo on 2019/12/16.
*/
@Slf4j
@Service
@Component
@Transactional
@BaseService
public class PmsOptLogServiceImpl extends BaseServiceImpl<PmsOptLogMapper, PmsOptLog, PmsOptLogExample> implements PmsOptLogService {

    @Autowired
    PmsOptLogMapper pmsOptLogMapper;

    @Override
    public int insert(PmsOptLog pmsOptLog) {
        return pmsOptLogMapper.insert(pmsOptLog);
    }

    @Override
    public PageInfo<PmsOptLog> getPmsOptLogs(PageQuery pageQuery) {
        PageHelper.startPage(pageQuery.getPage(),pageQuery.getSize());
        List<PmsOptLog> pmsOptLogLst = pmsOptLogMapper.getPmsOptLogs();
        return new PageInfo<>(pmsOptLogLst);
    }
}