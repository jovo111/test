package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.*;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsRoleUserExample;
import com.jovo.gateway.api.PmsRoleUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
* PmsRoleUserService实现
* Created by jovo on 2019/9/25.
*/
@Slf4j
@Service
@Component
@Transactional(rollbackFor = Exception.class)
@BaseService
@CacheConfig(cacheNames = "jovo-gateway-web-cache-pmss")
public class PmsRoleUserServiceImpl extends BaseServiceImpl<PmsRoleUserMapper, PmsRoleUser, PmsRoleUserExample> implements PmsRoleUserService {

    @Autowired
    PmsRoleUserMapper pmsRoleUserMapper;

    @Autowired
    PmsRolePmsMapper pmsRolePmsMapper;

    @Autowired
    PmsPermissionMapper pmsPermissionMapper;

    @Autowired
    PmsRoleMenuMapper pmsRoleMenuMapper;

    @Autowired
    PmsRoleMapper pmsRoleMapper;

    @Override
    public Set<String> selectPmsPermissionsByUid(Integer uid) {
        Integer roleId = pmsRoleUserMapper.selectPmsRoleIdsByUid(uid);
        String permissons = pmsRolePmsMapper.selectPmsPermissionsByRoleId(roleId);
        Set<String> pmsLst = pmsPermissionMapper.getPmsPermissionsByPmsId(permissons);
        return pmsLst;
    }

    @Override
    public Set<String> selectPmsRolesByUid(Integer uid) {
        return pmsRoleUserMapper.selectPmsRolesByUid(uid);
    }

    @Override
    public PmsRoleUser selectPmsRoleById(Integer id) {
        return pmsRoleUserMapper.selectPmsRolesById(id);
    }

    @Override
    public int update(PmsRoleUser pmsRoleUser) {
        return pmsRoleUserMapper.update(pmsRoleUser);
    }

    @Override
    public int delete(Integer id) {
        return pmsRoleUserMapper.delete(id);
    }

    @Override
    @Cacheable(key = "#p0")
    public List<PmsPermission> getPmsMenusByUserId(int uid) {
        Integer roleId = pmsRoleUserMapper.selectPmsRoleIdsByUid(uid);
        String menuIds = pmsRolePmsMapper.selectPmsPermissionsByRoleId(roleId);
        List<PmsPermission> menuLst = pmsPermissionMapper.getPmsMenusByPmsId(menuIds);

        return menuLst;
    }

    @Override
    public int updateByUserId(PmsRoleUser pmsRoleUser) {
        return pmsRoleUserMapper.updateByUserId(pmsRoleUser);
    }

    @Override
    public int insert(PmsRoleUser pmsRoleUser) {
        return pmsRoleUserMapper.insert(pmsRoleUser);
    }
}