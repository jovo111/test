package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsPermissionMapper;
import com.jovo.gateway.dao.model.PmsPermission;
import com.jovo.gateway.dao.model.PmsPermissionExample;
import com.jovo.gateway.api.PmsMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* PmsMenuService实现
* Created by jovo on 2019/11/29.
*/
@Slf4j
@Service
@Component
@Transactional(rollbackFor = Exception.class)
@BaseService
public class PmsMenuServiceImpl extends BaseServiceImpl<PmsPermissionMapper, PmsPermission, PmsPermissionExample> implements PmsMenuService {

    @Autowired
    PmsPermissionMapper pmsPermissionMapper;

    @Override
    public List<PmsPermission> getPmsMenus() {
        return pmsPermissionMapper.getPmsMenus();
    }

    @Override
    public PmsPermission selectPmsMenuByMenuId(Integer menuId) {
        return pmsPermissionMapper.selectPmsMenuByMenuId(menuId);
    }

    @Override
    public int insert(PmsPermission pmsPermission) {
        return pmsPermissionMapper.insert(pmsPermission);
    }

    @Override
    public int delete(Integer menuId) {
        return pmsPermissionMapper.delete(menuId);
    }

    @Override
    public int update(PmsPermission pmsPermission) {
        return pmsPermissionMapper.update(pmsPermission);
    }


}