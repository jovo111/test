package com.jovo.gateway.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jovo.common.annotation.BaseService;
import com.jovo.common.base.BaseServiceImpl;
import com.jovo.gateway.dao.mapper.PmsRoleUserMapper;
import com.jovo.gateway.dao.mapper.PmsUserMapper;
import com.jovo.gateway.dao.model.PmsRoleUser;
import com.jovo.gateway.dao.model.PmsUser;
import com.jovo.gateway.dao.model.PmsUserExample;
import com.jovo.gateway.api.PmsUserService;
import com.jovo.gateway.dao.query.page.PageQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * PmsUserService实现
 * Created by jovo on 2019/9/25.
 */
@Slf4j
@Service
@Component
@BaseService
@CacheConfig(cacheNames = "jovo-gateway-web-cache-user")
@Transactional(rollbackFor = Exception.class)
public class PmsUserServiceImpl extends BaseServiceImpl<PmsUserMapper, PmsUser, PmsUserExample> implements PmsUserService {

    @Autowired
    PmsUserMapper pmsUserMapper;

    @Autowired
    PmsRoleUserMapper pmsRoleUserMapper;

    @Override
    @Cacheable(key = "#p0")
    public PmsUser selectPmsUserByUserId(Integer uid) {
        return pmsUserMapper.selectPmsUserByUserId(uid);
    }

    @Override
    @Cacheable(key = "#p0")
    public PmsUser selectPmsUserByUserName(String uname) {
        return pmsUserMapper.selectPmsUserByUserName(uname);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int insert(PmsUser pmsUser) {
        int count = pmsUserMapper.insert(pmsUser);

        if (count > 0) {
            return pmsUser.getUserId();
        }
        return 0;
    }

    @Override
    @CacheEvict(allEntries = true)
    public int update(PmsUser pmsUser) {
        return pmsUserMapper.update(pmsUser);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int delete(Integer uid) {
        return pmsUserMapper.delete(uid);
    }

    @Override
    @Cacheable
    public PageInfo<PmsUser> getUsersByExample(PageQuery pageQuery) {
        PageHelper.startPage(pageQuery.getPage(),pageQuery.getSize());
        List<PmsUser> pmsUserLst = pmsUserMapper.getUsersByExample();
        return new PageInfo<>(pmsUserLst);
    }

    @Override
    @CacheEvict(allEntries = true)
    public int insertUserRole(PmsUser pmsUser, PmsRoleUser pmsRoleUser) {
        int count = pmsUserMapper.insert(pmsUser);
        if (count > 0) {
            pmsRoleUser.setUserId(pmsUser.getUserId());
            count = pmsRoleUserMapper.insert(pmsRoleUser);

            return count;
        }
        return 0;
    }
}