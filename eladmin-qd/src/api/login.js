import request from '@/utils/request'
import store from '@/store'

const baseUrl = process.env.SSO_URL

export function login(username, password, code, uuid) {
  return request({
    url: baseUrl + '/auth/login',
    method: 'post',
    data: {
      username,
      password,
      code,
      uuid
    }
  })
}

export function logout() {
  return request({
    url: baseUrl + '/auth/logout',
    method: 'post',
    data: {
      token: store.getters.token,
      username: store.getters.user.userName
    }
  })
}

export function getInfo() {
  return request({
    url: baseUrl + '/admin/users/info',
    method: 'get'
  })
}

export function getCodeImg() {
  return request({
    url: baseUrl + '/auth/vCode',
    method: 'get'
  })
}
