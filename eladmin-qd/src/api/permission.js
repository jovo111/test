import request from '@/utils/request'

const baseUrl = process.env.SSO_URL

// 获取所有的权限树
export function getPermissionTree() {
  return request({
    url: baseUrl + '/admin/pmss/tree',
    method: 'get'
  })
}

export function downloadMenu(params) {
  return request({
    url: baseUrl + '/admin/menus/download',
    method: 'get',
    params,
    responseType: 'blob'
  })
}

export function add(data) {
  return request({
    url: baseUrl + '/admin/pmss',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: baseUrl + '/admin/pmss/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: baseUrl + '/admin/pmss',
    method: 'put',
    data
  })
}
