import request from '@/utils/request'

const baseUrl = process.env.SSO_URL

export function add(data) {
  return request({
    url: baseUrl + '/admin/users',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: baseUrl + '/admin/users/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: baseUrl + '/admin/users',
    method: 'put',
    data
  })
}

export function updatePass(user) {
  const data = {
    oldPass: user.oldPass,
    newPass: user.newPass
  }
  return request({
    url: baseUrl + '/admin/users/updatePass/',
    method: 'post',
    data
  })
}

export function updateEmail(code, data) {
  return request({
    url: baseUrl + '/admin/users/updateEmail/' + code,
    method: 'post',
    data
  })
}

