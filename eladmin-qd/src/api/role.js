import request from '@/utils/request'

const baseUrl = process.env.SSO_URL

// 获取所有的Role
export function getAll() {
  return request({
    url: baseUrl + '/admin/roles',
    method: 'get'
  })
}

// 用于选择器使用
export function getAllBySelect() {
  return request({
    url: baseUrl + '/admin/roles/build',
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: baseUrl + '/admin/roles',
    method: 'post',
    data
  })
}

export function get(id) {
  return request({
    url: baseUrl + '/admin/roles/' + id,
    method: 'get'
  })
}

export function getLevel() {
  return request({
    url: baseUrl + '/admin/roles/level',
    method: 'get'
  })
}

export function del(id) {
  return request({
    url: baseUrl + '/admin/roles/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: baseUrl + '/admin/roles',
    method: 'put',
    data
  })
}

export function editPermission(data) {
  return request({
    url: baseUrl + '/admin/roles/permission',
    method: 'put',
    data
  })
}

export function editMenu(data) {
  return request({
    url: baseUrl + '/admin/roles/menu',
    method: 'put',
    data
  })
}
