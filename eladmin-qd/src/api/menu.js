import request from '@/utils/request'

const baseUrl = process.env.SSO_URL

// 获取所有的菜单树
export function getMenusTree() {
  return request({
    url: baseUrl + '/admin/menus/build',
    method: 'get'
  })
}

export function buildMenus() {
  return request({
    url: baseUrl + '/admin/menus/build',
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: baseUrl + '/admin/menus',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: baseUrl + '/admin/menus/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: baseUrl + '/admin/menus',
    method: 'put',
    data
  })
}
